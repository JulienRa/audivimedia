# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2016-02-15 17:06+0300\n"
"PO-Revision-Date: 2016-02-15 21:17+0300\n"
"Language-Team: EMAIL@ADDRESS\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.7\n"
"Last-Translator: \n"
"Language: en_US\n"

#: View/Elements/element_login.ctp:3
msgid "identifiant"
msgstr ""

#: View/Elements/element_login.ctp:4
msgid "Identifiant:"
msgstr ""

#: View/Elements/element_login.ctp:6
msgid "mot de passe"
msgstr ""

#: View/Elements/element_login.ctp:7
msgid "Mot de passe"
msgstr ""

#: View/Elements/element_login.ctp:9
msgid "Connexion"
msgstr ""

#: View/Elements/element_login.ctp:9
msgid "Créer un compte"
msgstr ""

#: View/Elements/element_login.ctp:14
msgid "Mot de passe oublié"
msgstr ""

#: View/Elements/element_footer.ctp:15
msgid "Centre auditif Audivi"
msgstr ""

#: View/Elements/element_footer.ctp:16
msgid "Mentions legales"
msgstr ""

#: View/Elements/element_footer.ctp:18
msgid "Conditions générales de vente"
msgstr ""

#: View/Elements/element_footer.ctp:20
msgid "Contacte"
msgstr ""

#: View/Layouts/default.ctp:29
msgid "Deconnection"
msgstr ""

#: View/Layouts/default.ctp:34
msgid "Configuration"
msgstr ""

#: View/Layouts/default.ctp:36
msgid "Mes Commandes"
msgstr ""

#: View/Layouts/default.ctp:41
msgid "Mes logiciels"
msgstr ""

#: View/Layouts/default.ctp:44
msgid "Mon compte"
msgstr ""

#: View/Layouts/default.ctp:60
msgid "Accueil"
msgstr ""

#: View/Layouts/default.ctp:61
msgid "Logiciels Famille"
msgstr ""

#: View/Layouts/default.ctp:62
msgid "Logiciels Professionnels"
msgstr ""

#: View/Layouts/default.ctp:64
msgid "Audioprothesistes"
msgstr ""

#: View/Layouts/default.ctp:65
msgid "Orl & Phoniatres"
msgstr ""

#: View/Layouts/default.ctp:66
msgid "Orthophonistes"
msgstr ""

#: View/Layouts/default.ctp:67
msgid "Logiciels adapté à votre métier"
msgstr ""

#: View/Layouts/default.ctp:70
msgid "Commandes"
msgstr ""

#: View/Layouts/default.ctp:71
msgid "En savoir plus"
msgstr ""

#: View/Layouts/default.ctp:71
msgid "Contact"
msgstr ""

#: View/Commandes/index.ctp:9
msgid "Passez votre commande"
msgstr ""

#: View/Commandes/index.ctp:10
msgid "Choisissez vos logiciels"
msgstr ""

#: View/Commandes/index.ctp:13
msgid "Les livraisons"
msgstr ""

#: View/Commandes/index.ctp:14
msgid "sont assur&eacute;es"
msgstr ""

#: View/Commandes/index.ctp:15
msgid "sur la France"
msgstr ""

#: View/Commandes/index.ctp:16
msgid "m&eacute;tropolitaine"
msgstr ""

#: View/Commandes/index.ctp:17
msgid "pour 5 &euro;"
msgstr ""

#: View/Commandes/index.ctp:19
msgid "TTC pour un colis"
msgstr ""

#: View/Commandes/index.ctp:20
msgid "n\'&eacute;xc&eacute;dant "
msgstr ""

#: View/Commandes/index.ctp:21
msgid "pas 300 grammes (3 coffrets)."
msgstr ""

#: View/Commandes/index.ctp:23
msgid "Voir les conditions"
msgstr ""

#: View/Commandes/index.ctp:23
msgid "générales de vente"
msgstr 

#: View/Commandes/index.ctp:46
msgid "Support logiciel &agrave;"
msgstr ""

#: View/Commandes/index.ctp:113
msgid "Passer &agrave; l&rsquo;&eacute;tape suivante"
msgstr ""

#: View/Logiciels/audioprothesistes.ctp:4
msgid "Logiciels pour les audioproth&eacute;sistes"
msgstr ""

#: View/Logiciels/audioprothesistes.ctp:4
msgid "En savoir plus"
msgstr ""

#: View/Logiciels/audioprothesistes.ctp:7
msgid "Cr&eacute;&eacute;"
msgstr ""

#: View/Logiciels/audioprothesistes.ctp:8
msgid "pour les audioproth&eacute;sistes"
msgstr ""

#: View/Logiciels/audioprothesistes.ctp:9
msgid "par des audioproth&eacute;sistes !"
msgstr ""

#: View/Logiciels/audioprothesistes.ctp:42
msgid "Essayer ce logiciel"
msgstr ""

#: View/Logiciels/orlphoniatres.ctp:4
msgid "Logiciels pour ORL et phoniatres"
msgstr ""

#: View/Logiciels/orlphoniatres.ctp:7
msgid "Des outils simples et ergonomiques &agrave;"
msgstr ""

#: View/Logiciels/orlphoniatres.ctp:8
msgid "avoir sous la main pour vos bilans "
msgstr ""

#: View/Logiciels/orlphoniatres.ctp:9
msgid "audiom&eacute;triques chez "
msgstr ""

#: View/Logiciels/orlphoniatres.ctp:10
msgid "l\'adulte et l\'enfant !"
msgstr ""

#: View/Logiciels/orlphoniatres.ctp:43
msgid "Essayer ce logiciel"
msgstr ""

#: View/Logiciels/orlphoniatres.ctp:60
msgid "Acheter"
msgstr ""

#: View/Logiciels/orthophonistes.ctp:4
msgid "Logiciels pour ORL et phoniatre"
msgstr ""

#: View/Logiciels/orthophonistes.ctp:7
msgid "Des outils simples et ergonomiques &agrave; "
msgstr ""

#: View/Logiciels/orthophonistes.ctp:8
msgid "avoir sous la main pour vos bilans "
msgstr ""

#: View/Logiciels/orthophonistes.ctp:9
msgid "audiom&eacute;triques chez"
msgstr ""

#: View/Logiciels/orthophonistes.ctp:10
msgid "l\'adulte et l\'enfant !"
msgstr ""

#: View/Logiciels/orthophonistes.ctp:50
msgid "Acheter"
msgstr ""

#: View/Logiciels/bycategory.ctp:4
msgid "Logiciels pour les audioproth&eacute;sistes"
msgstr ""

#: View/Logiciels/bycategory.ctp:7
msgid "Cr&eacute;&eacute; pour les audioproth&eacute;sistes"
msgstr ""

#: View/Logiciels/bycategory.ctp:7
msgid "par des audioproth&eacute;sistes !"
msgstr ""

#: View/Logiciels/bycategory.ctp:12
msgid "Par"
msgstr ""

#: View/Logiciels/bycategory.ctp:14
msgid "audiproth&eacute;siste membre du Coll&egrave;ge"
msgstr ""

#: View/Logiciels/bycategory.ctp:15
msgid "National d\'Audioproth&egrave;se, &eacute;galement "
msgstr ""

#: View/Logiciels/bycategory.ctp:16
msgid "fondateur du centre auditif "
msgstr ""