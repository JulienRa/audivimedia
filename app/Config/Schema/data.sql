-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 22 Février 2016 à 16:33
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `cakecart`
--

-- --------------------------------------------------------

--
-- Structure de la table `autres`
--

CREATE TABLE IF NOT EXISTS `autres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detail_id` int(11) NOT NULL,
  `image` varchar(22) NOT NULL,
  `prix` int(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='produits informatique tel que cd, casques,...' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=7 ;

--
-- Contenu de la table `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `active`, `created`, `modified`) VALUES
(1, 'Burton', 'burton', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(2, 'Celtek', 'celtek', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(3, 'Dakine', 'dakine', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(4, 'DC', 'dc', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(5, 'Electric', 'electric', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(6, 'Forum', 'forum', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `carts`
--

CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sessionid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` decimal(6,2) DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `weight_total` decimal(6,2) DEFAULT NULL,
  `subtotal` decimal(6,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `lft` int(10) unsigned DEFAULT NULL,
  `rght` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_fr` text COLLATE utf8_unicode_ci,
  `description_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description_es` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `lft`, `rght`, `name`, `slug`, `description_fr`, `description_en`, `description_es`, `created`, `modified`) VALUES
(1, NULL, 1, 18, 'Pricipal', 'main', 'main', '', '', '2016-02-05 11:47:38', '2016-02-05 11:47:38'),
(2, 1, 2, 3, 'Logiciels Famille', 'famille', 'Aidez votre enfant dÃ©ficient auditif\ndans son apprentissage de la langue', '', '', '2016-02-05 11:48:57', '2016-02-05 11:48:57'),
(3, 1, 4, 17, 'Logiciels Professionnels', 'professionnels', 'Augmentez votre efficacitÃ©,\r\ndÃ©couvrez vos nouveaux outils', '', '', '2016-02-05 11:49:46', '2016-02-05 11:49:46'),
(4, 3, 11, 12, 'ORL et Phoniatres', 'phoniatres', 'Des outils simples et ergonomiques Ã  avoir sous la main pour vos bilans audiomÃ©triques chez l''adulte et l''enfant ! ', '', '', '2016-02-09 12:52:13', '2016-02-09 12:52:13'),
(5, 3, 13, 14, 'Orthophonistes', 'orthophonistes', 'Parce que la rÃ©Ã©ducation auditive passe aussi par le jeu ! BibliothÃ¨ques sonores, jeux, apprentissages ... ', '', '', '2016-02-09 12:55:47', '2016-02-09 12:55:47'),
(6, 3, 15, 16, 'AudioprothÃ©sistes', 'audioprothesistes', 'Ayez sous la main les outils informatiques nÃ©cessaires Ã  parfaire vos bilans prÃ© et post-appareillage ! ', '', '', '2016-02-09 12:56:16', '2016-02-09 12:56:16');

-- --------------------------------------------------------

--
-- Structure de la table `details`
--

CREATE TABLE IF NOT EXISTS `details` (
  `category_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logiciel_id` int(10) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `details`
--

INSERT INTO `details` (`category_id`, `product_id`, `id`, `logiciel_id`, `page_id`) VALUES
(2, 18, 1, 18, 0),
(3, 18, 2, 18, 0),
(2, 20, 3, 20, 0),
(2, 19, 6, 19, 0),
(6, 21, 7, 21, 0),
(6, 22, 8, 22, 0),
(6, 23, 9, 23, 0),
(6, 24, 10, 24, 0),
(4, 21, 12, 21, 0),
(4, 22, 13, 22, 0);

-- --------------------------------------------------------

--
-- Structure de la table `dtmedia`
--

CREATE TABLE IF NOT EXISTS `dtmedia` (
  `product_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `logiciels`
--

CREATE TABLE IF NOT EXISTS `logiciels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `brand_id` int(11) unsigned DEFAULT NULL,
  `name_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_fr` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'short description',
  `about_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `about_es` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_es` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_max` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `try` tinyint(1) NOT NULL DEFAULT '0',
  `trydur` int(2) NOT NULL DEFAULT '10',
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `prompted` tinyint(1) NOT NULL DEFAULT '0',
  `cdrom` tinyint(1) NOT NULL,
  `usb` tinyint(1) NOT NULL,
  `telechargement` tinyint(1) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `mac` tinyint(1) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name_fr`),
  KEY `modified` (`modified`),
  KEY `name_slug` (`slug`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Logiciels informatique' AUTO_INCREMENT=25 ;

--
-- Contenu de la table `logiciels`
--

INSERT INTO `logiciels` (`id`, `category_id`, `brand_id`, `name_fr`, `name_en`, `name_es`, `slug`, `about_fr`, `about_en`, `about_es`, `description_fr`, `description_en`, `description_es`, `image`, `image_max`, `price`, `try`, `trydur`, `tags`, `views`, `active`, `created`, `modified`, `prompted`, `cdrom`, `usb`, `telechargement`, `win`, `mac`, `age`) VALUES
(18, 4, 4, 'Le monde sonore d''Otto', 'Otto''s world ', 'El mundo sonoror de Otto', 'dc-rogan-snowboard-boots-black-rasta', 'Stimule le développement sensoriel et améliore les facultés auditives !', '', '', 'Jeu de découverte sonore et d’éducation auditive.', '', '', 'img_8.png', 'img_1.2.png', '30', 0, 10, NULL, 1179, 1, '2012-12-06 00:00:00', '2016-02-10 07:34:32', 1, 0, 0, 0, 0, 0, 0),
(19, 4, 6, 'La vache et le chevalier', 'The cow and the knight', 'La vaca y el caballo', 'forum-aura-snowboard-boots-chocolate', 'Aide les enfants sourds dans leur maitrise de la lange Française. Histoire sous-titrée et condé en LPC. ', '', '', 'Aide à l''acquisition de la langue française. A l''attention des enseignants.', '', '', 'img_7.png', 'img_2.2.png', '302', 0, 10, NULL, 1199, 1, '2012-12-06 00:00:00', '2016-02-18 13:36:27', 1, 0, 0, 0, 0, 0, 0),
(20, 4, 4, 'Cles en main ', 'Keys in hand', '\nLlavero', 'dc-lear-mittens-blue-radiance-black', '\n	\nFamiliarise efficacement les adultes débutants et les enfants au code LPC sous la forme d''un jeu de cartes. ', '', '', 'Entrainement et familiarisation au code LCP', '', '', 'img_6.png', 'img_3.2.png', '30', 0, 10, NULL, 1138, 1, '2012-12-06 00:00:00', '2016-02-10 07:33:56', 1, 0, 0, 0, 0, 0, 0),
(21, 3, NULL, 'Digivox', 'Digivox', 'Digivox', 'Digivox', 'L''audiométrie vocale en toute fluidité !', '', '', 'Logiciel très complet et ultra ergonomique pour la passation d''audiométries vocales avec ou sans bruit de fond, de la stéréo au 7.1 Digivox contient nativement toutes les listes validées par le Collè National d''Audioprothèse et se veut être l''outil à avoir en permanence sous la main pour la réalisation d''audiométries vocales. Petit, simple et ultra complet !\r\n', '', '', NULL, 'img-aud_1.png', '375', 1, 10, NULL, 5, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(22, 3, NULL, 'VideoShow', 'VideoShow', 'VideoShow', 'VidéoShow', 'Support logiciel à l''audiométrie pédiatrique.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_2.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(23, 3, NULL, 'La Terrasse', '\nThe terrace', '\nLa terraza', 'La Terrasse', 'Tests auditifs supraliminaires.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_4.png', '375', 0, 10, NULL, 1, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(24, 3, NULL, 'La souris bleue', '\nBlue mouse', 'ratón azul', 'La souris bleue', 'L''imagier sonore pour l''éveil auditif !', '', '', 'L''imagier sonore La Souris bleue est un logiciel d''éveil auditif et d''évaluation des capacités auditives. Il peut être utilisé comme matériel pédagogique (par les enseignants, les orthophonistes, les éducateurs spécialisés) ou comme matériel d''évaluation des capacités de reconnaissance auditive, sonore et vocale par les professionnels de l''audiophonologie.', '', '', NULL, 'img-aud_3.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `media`
--

INSERT INTO `media` (`id`, `name`) VALUES
(1, 'img1.png'),
(2, 'img2.png'),
(3, 'img3.png'),
(4, 'img4.png'),
(5, 'img5.png'),
(6, 'img6.png'),
(7, 'img7.png'),
(8, 'img8.png');

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` decimal(8,2) unsigned DEFAULT '0.00',
  `order_item_count` int(11) DEFAULT NULL,
  `subtotal` decimal(8,2) DEFAULT NULL,
  `tax` decimal(8,2) DEFAULT NULL,
  `shipping` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) unsigned DEFAULT NULL,
  `order_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorization` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `weight` decimal(8,2) unsigned DEFAULT '0.00',
  `price` decimal(8,2) unsigned DEFAULT NULL,
  `subtotal` decimal(8,2) unsigned DEFAULT NULL,
  `productmod_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `lft` int(10) NOT NULL,
  `rght` int(10) NOT NULL,
  `body_fr` text NOT NULL,
  `body_en` text NOT NULL,
  `body_es` text NOT NULL,
  `title` varchar(150) NOT NULL,
  `autor` varchar(150) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `logiciel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `lft`, `rght`, `body_fr`, `body_en`, `body_es`, `title`, `autor`, `active`, `product_id`, `logiciel_id`) VALUES
(1, 0, 1, 28, 'Principale', 'Main', '?', 'Main', 'Admin', 0, 0, 0),
(5, 1, 8, 9, 'Stimuler le dÃ©veloppement sensoriel des enfants et amÃ©liorer leurs facultÃ©s auditives en leur faisant dÃ©couvrir et identifier des sons.\r\n\r\nUn moment privilÃ©giÃ© parents-enfant pour dÃ©couvrir la richesse de l''univers sonore.\r\n\r\nTestÃ© et approuvÃ© par des enfants dÃ©ficients auditifs et leur famille ! ', 'Stimulates the sensorial development of children and improve their hearing skills by making\r\nthem discover and identify sounds.\r\n\r\nA special parent-child experience aimed at discovering the richness of the universe of sound.\r\n\r\nTested and approved by hearing-Â­impaired kids and their families!', 'Estimular el desarrollo sensorial de los niÃ±os y mejorar sus facultades auditivas haciÃ©ndoles descubrir e identificar sonidos.\r\n\r\nUn momento privilegiado entre padres y niÃ±os para descubrir el universo sonoro.\r\n\r\nÂ¡Probado y aprobado por niÃ±os con discapacidad auditiva y sus familias!', 'But', '', 0, 0, 18),
(6, 1, 10, 11, 'La vache et le chevalier a pour vocation dâ€™apporter une aide aux enfants sourds dans leur maÃ®trise de la langue franÃ§aise. ', 'timulates the sensorial development of children and improve their hearing skills by making\r\nthem discover and identify sounds.\r\n\r\nA special parent-child experience aimed at discovering the richness of the universe of sound.\r\n\r\nTested and approved by hearing-Â­impaired kids and their families!', 'Estimular el desarrollo sensorial de los niÃ±os y mejorar sus facultades auditivas haciÃ©ndoles descubrir e identificar sonidos.\r\n\r\nUn momento privilegiado entre padres y niÃ±os para descubrir el universo sonoro.\r\n\r\nÂ¡Probado y aprobado por niÃ±os con discapacidad auditiva y sus familias! ', 'Objectif', '', 0, 0, 19),
(7, 1, 12, 13, 'Stimule le dÃ©veloppement sensoriel des enfants et amÃ©liore leurs facultÃ©s auditives en leur faisant dÃ©couvrir et identifier des sons.\r\n\r\nOrganisÃ© sous forme de jeu, Le monde sonore d''Otto propose une grande variÃ©tÃ© d''environnements sonores de la vie quotidienne (la maison, la rue, le bord de mer, etc.).\r\nChaque environnement prÃ©sente une sÃ©rie de sons Ã  dÃ©couvrir, puis Ã  mÃ©moriser et Ã  reconnaÃ®tre Ã  travers plusieurs jeux. Les sons sont reprÃ©sentÃ©s Ã  l''Ã©cran par des images accompagnÃ©es du vocabulaire Ã©crit et prononcÃ©.', 'This educational game stimulates the sensory development of children and improves their hearing skills by making them discover and identify sounds.\r\n\r\nOtto''s World Of Sound offers a great variety of daily life sound environments (the home, the street, the seaside,etc.). Each environment presents a series of sounds to discover, memorize and recognize throughout several games.\r\nThe sounds are represented on screen by pictures together with written word and pronunciation.', 'Estimula el desarrollo sensorial de los niÃ±os y mejora sus facultades auditivas haciÃ©ndoles descubrir e identificar sonidos.\r\n\r\nEl mundo sonoro de Otto ofrece una gran variedad de entornos sonoros de la vida cotidiana (la casa, la calle, la orilla del mar, etc.).\r\n\r\nCada entorno contiene una serie de sonidos a descubrir, recordar y reconocer a travÃ©s de varios juegos. Los sonidos son representados en la pantalla por imÃ¡genes acompaÃ±adas del vocabulario escrito y oral. ', 'Descriptif', '', 0, 0, 18),
(9, 1, 16, 17, 'Le monde sonore d''Otto a Ã©tÃ© dÃ©veloppÃ© par AudivimÃ©dia avec l''aide de la firme danoise Oticon dans le cadre de son programme OtiKids.', 'Prerequisites : Children eager to discover sounds and/or in need of auditory re-education. ', '', 'Info Pratique', '', 0, 0, 18),
(10, 1, 18, 19, 'DÃ©vÃ©dÃ©rom conÃ§u spÃ©cifiquement pour les enfants sourds, autour dâ€™une histoire transposÃ©e en trois niveaux de langue, qui leur permet une progression par paliers.\r\n\r\nRÃ©alisÃ© avec lâ€™aide du MinistÃ¨re de lâ€™Education nationale.\r\n\r\nLâ€™histoire de "La vache et le chevalier" et les parties annexes du dÃ©vÃ©dÃ©rom sont sous-titrÃ©es, codÃ©es en LPC, signÃ©es (pour le premier niveau de la langue uniquement) et illustrÃ©es de nombreux visuels aidant Ã  la comprÃ©hension.\r\n\r\nUne partie lexicale et syntaxique se rÃ©fÃ©rant Ã  lâ€™histoire ainsi quâ€™un thÃ¨me dÃ©veloppÃ© sur "lâ€™ombre et la lumiÃ¨re" sont proposÃ©s sur la partie interactive du dÃ©vÃ©dÃ©rom. ', 'This interactive DVD features a story transposed in three levels of language, which allows children to move progressively through the DVD.\r\n\r\nCreated with the support of the National Ministry of Education.\r\n\r\nThe story "La Vache et le Chevalier" and the additional parts of the DVD are subtitled, LPC coded, and illustrated with numerous images for a better understanding.\r\n\r\nThe interactive rubric of the DVD includes a lexical and syntactic story related part and a theme developed around "Light and Shadow". ', 'El DVD-ROM estÃ¡ diseÃ±ado en torno a una historia estructurada en tres niveles de lenguaje, que permite a los niÃ±os progresar por etapas.\r\n\r\nRealizado con la ayuda del Ministerio de EducaciÃ³n Nacional.\r\n\r\nLa historia de â€œLa vaca y el caballeroâ€ y las partes anexas del DVD-ROM estÃ¡n subtituladas, codificadas en LPC, suscritas (Ãºnicamente para el primer nivel de lengua) e ilustradas con numerosas imÃ¡genes que ayudan a la comprensiÃ³n.\r\n\r\nEn la parte interactiva del DVD-ROM se encuentran disponibles una parte lÃ©xica y sintÃ¡ctica en referencia a la historia, asÃ­ como un tema desarrollado sobre â€œlas luces y las sombrasâ€ ', 'Descriptif', '', 0, 0, 19),
(11, 1, 20, 21, ' Les â€œLivres complÃ©tÃ©sâ€ constituent une aide pour les enseignants, en leur apportant un support adaptÃ© Ã  la surditÃ© de lâ€™enfant, afin de favoriser la maÃ®trise de la langue franÃ§aise chez leurs Ã©lÃ¨ves sourds profonds ou malentendants : les rÃ©cits sont progressifs, prÃ©cisÃ©ment illustrÃ©s, les modes de communication sont spÃ©cifiques (code LPC*, LSF*, sous-titrage... ).\r\n\r\nLes â€œLivres complÃ©tÃ©sâ€ ont pour base une histoire proposÃ©e en trois niveaux de langue, donnant la possibilitÃ© dâ€™une progression par paliers. Dans les â€œLivres complÃ©tÃ©sâ€, les illustrations suivent prÃ©cisÃ©ment les textes pour faciliter lâ€™accÃ¨s au sens.\r\n\r\nLe cÃ©dÃ©rom interactif de â€œLa vache et le chevalierâ€, bien quâ€™il soit conÃ§u spÃ©cifiquement pour les enfants sourds profonds et malentendants - donc entiÃ¨rement sous-titrÃ© et complÃ©tÃ© par le code LPC (Langue franÃ§aise ParlÃ©e ComplÃ©tÃ©e), est un support qui peut Ãªtre montrÃ© Ã  toute la classe.\r\n\r\nDans les â€œLivres complÃ©tÃ©sâ€ , les adaptations au handicap de la surditÃ© ne sont pas systÃ©matiquement imposÃ©es : elles constituent des options, permettant ainsi une souplesse dans les modalitÃ©s proposÃ©es par lâ€™enseignant selon le profil de lâ€™enfant et selon ses compÃ©tences en langue franÃ§aise et en code LPC.\r\n\r\nUn complÃ©ment lexical et le dÃ©veloppement dâ€™un thÃ¨me rÃ©aliste viennent enrichir lâ€™histoire (ici, le thÃ¨me porte sur â€œLâ€™ombre et la lumiÃ¨reâ€).\r\n\r\nA terme, Les â€œLivres complÃ©tÃ©sâ€ doivent constituer une collection destinÃ©e Ã  des Ã©lÃ¨ves sourds profonds ou malentendants, de la maternelle au CM2.\r\nInfos pratiques\r\n\r\nLe dÃ©vÃ©dÃ©rom contient Ã©galement un guide pÃ©dagogique et des activitÃ©s autour du thÃ¨me de lâ€™ombre et de la lumiÃ¨re.\r\n\r\n!\r\n\r\nPrÃ©requis : Jeunes enfants sourds profonds ou malentendants.\r\n\r\n5/10\r\nans\r\n\r\nSelon le niveau en franÃ§ais de lâ€™enfant.\r\n\r\nPC\r\n\r\nConfiguration minimum : Pentium II 350 Mhz, 64Mo de RAM, Windows XP, Vista, seven et 8, carte vidÃ©o 16 Mo, carte son, lecteur DVD-ROM, Ã©cran 1024 x 768\r\nDes logiciels adaptÃ©s pour votre enfant sourd !\r\nLe monde sonore d''Otto\r\n\r\nStimule le dÃ©veloppement sensoriel et amÃ©liore les facultÃ©s auditives !en savoir plus\r\n\r\nJeu de dÃ©couverte sonore et dâ€™Ã©ducation auditive.\r\n\r\n2+\r\nans\r\nLe monde sonore d''Otto\r\nLa vache et le chevalier\r\n\r\nAide les enfants sourds dans leur maÃ®trise de la langue franÃ§aise. Histoire sous-titrÃ©e et codÃ©e en LPC.en savoir plus\r\n\r\nAide Ã  l''acquisition de la langue franÃ§aise. A l''attention des enseignants.\r\n\r\n5/10\r\nans\r\nLa vache et le chevalier\r\nClÃ©s en main\r\n\r\nFamiliarise efficacement les adultes dÃ©butants et les enfants au code LPC sous la forme dâ€™un jeu de cartes.en savoir plus\r\n\r\nFamiliarisation au code LPC.\r\n\r\n5+\r\nans\r\nClÃ©s en main\r\n', '"The ""completed books"" constitute an aid for the teachers, providing them with relevant support adapted to the deafness of the child, with the purpose of assisting the students with profound deafnessor hearing impairment in the learning of the French language: the stories are progressive, adequately illustrated, and the ways of communication are specific (LPC* code, LSF*, subtitles...)\r\n\r\nThe ""Completed Books"" are based on a story proposed in three levels of language, offering the possibility of a progression by level. In the ""Completed Books"", the illustrations follow the texts in a precise way to facilitate learning.\r\n\r\nThe Cow And The Knight interactive DVD has been designed specifically for children with profound deafness or hearing impairment therefore, it is subtitled and complemented with LPC code, being a support that can be shown to the entire class.\r\n\r\nIn the ""Completed Books"", the adaptations for deafness are not systematically imposed: they are featured as options, allowing for flexibility in the modalities offered by the teacher, according to the child''s profile and their skills with the French language and LPC code.\r\n\r\nA lexical part and the development of a realistic topic, help\r\nenhance the story (in this case, the realistic topic is that of ""The lights and the shadows"").\r\n\r\nFinally, the ""Completed Books"" should constitute a collection for students with profound deafness or hearing impairment, from preâ€school to elementary. "', 'Los â€œlibros completadosâ€ constituyen una ayuda para los profesores, proporcionando les, un buen soporte, adaptado a las necesidades de los niÃ±os con deficiencia auditiva, con el fin de favorecer el aprendizaje y dominio de la lengua espaÃ±ola. Les histÃ³rias, estÃ¡n estructuradas de manera progresiva, estÃ¡n ilustradas de una manera muy precisa e incluyen subtÃ­tulos, cÃ³digo LPC y LSF.\r\n\r\nEl DVD-ROM interactivo de â€œLa vaca y el caballeroâ€, ha sido diseÃ±ado especÃ­ficamente para los niÃ±os con sordera profunda o discapacidad auditiva, por tanto, estÃ¡n subtitulados y complementados con el cÃ³digo LPC, es un apoyo que puede ser mostrado a toda la clase.\r\n\r\nEn los â€œLibros completadosâ€ , las adaptaciones a la discapacidad de la sordera no estÃ¡n sistemÃ¡ticamente impuestas: hay diferentes opciones que permiten una flexibilidad en las modalidades ofrecidas por el docente, segÃºn el perfil del niÃ±o y segÃºn sus capacidades en la lengua espaÃ±ola y en el cÃ³digo LPC.\r\n\r\nUn complemento lÃ©xico y el desarrollo de un tema realista enriquecen la historia (aquÃ­, el tema trata sobre â€œLas luces y las sombrasâ€).\r\n\r\nFinalmente, Los â€œLibros completadosâ€ deben constituir una colecciÃ³n destinada a los alumnos con sordera profunda o discapacidad, desde infantil hasta primaria.', 'Detail', '', 0, 0, 19),
(12, 1, 22, 23, 'ClÃ©s en main est un outil spÃ©cifique pour familiariser et entraÃ®ner les enfants sourds Ã  la pratique du Code LPC. ', 'Ready To Go is a specific tool used in familiarisation training for both beginner adults and deaf children in the practice of the LPC Code (this code helps the deaf learn the French language).', 'Listo para usar es una herramienta para familiarizar y entrenar a los niÃ±os sordos en la prÃ¡ctica del CÃ³digo LPC ', 'Objectif', '', 0, 0, 20),
(13, 1, 24, 25, 'ClÃ©s en main se prÃ©sente sous la forme de jeux de cartes affichant les phonÃ¨mes. Ils sont Ã©crits ou bien codÃ©s par une tÃªte virtuelle.\r\nVous pourrez pratiquer le codage ou le dÃ©codage du code LPC, avec ou sans la vocalisation des phonÃ¨mes.\r\n\r\nClÃ©s en main stimule le dÃ©veloppement sensoriel de ces enfants et amÃ©liore leurs facultÃ©s auditives en leur faisant dÃ©couvrir et identifier des sons. ', 'Ready To Go is a card game depicting phonemes, which are written or modified by the program. This allows the user to practice coding or decoding the LPC code, with or without vocalizing the phonemes.\r\n\r\nReady To Go stimulates the sensory development of children and helps improve their hearing skills, making them discover and identify sounds. ', 'Listo para usar se presenta en forma de juegos de cartas que muestra fonemas. EstÃ¡n escritos o bien modificados por una cabeza virtual.\r\n\r\nUsted podrÃ¡ practicar la codificaciÃ³n o descodificaciÃ³n del cÃ³gido LPC, con o sin vocalizaciÃ³n des los fonemas.\r\n\r\nListos para usar estimula el desarrollo sensorial de estos niÃ±os y mejora sus facultades y mejora sus facultades auditivas haciÃ©ndole descubrir e identificar sonidos. ', 'Descriptif', '', 0, 0, 20),
(14, 1, 26, 27, 'ClÃ©s en main sâ€™organise selon une progression simple qui commence avec les positions des voyelles sur le visage, pour arriver aux syllabes ("consonne LPC" + "voyelle LPC") et aux mots.\r\n\r\nPour des exercices progressifs ClÃ©s en main offre la possibilitÃ© de ne travailler quâ€™avec certaines voyelles ou certaines consonnes.\r\n\r\nVous pouvez composer des suites de voyelles ou de syllabes, choisir le nombre de cartes (donc le nombre de "clÃ©s LPC" pour les exercices), accÃ©der Ã  des tableaux rÃ©capitulatifs, choisir un mot dans une listeâ€¦\r\n\r\nVous avez aussi le choix entre le mode silencieux ou sonore (exceptÃ© pour les exercices sur les mots qui sont toujours silencieux).\r\n\r\nAvec ClÃ©s en main, vous avez un outil complet pour faire progresser rapidement votre enfant. ', '"Ready To Go is organized according to a simple progression that starts with the position of the vowels on a face, followed by the syllables (""LPC consonant"" and ""LPC vowel"") and finally the words.\r\n\r\nFor progressive exercises, Ready To Go offers the possibility to work only with a given set of vowels and consonants.\r\n\r\nReady To Go offers the possibility to form sequences of vowels and syllables, choose the number of cards (according to the number of ""LPC keys"" for the exercises), access summary tables, choose a word from a listâ€¦\r\n\r\nIt also allows the user to choose between the silent or sound mode (except for the exercises with words that are always silent).\r\n\r\nWith Ready To Go you will have a thorough tool to help your child progress rapidly."\r\n\r\n', ' Listo para usar se organiza segÃºn una progresiÃ³n simple que comienza con las posiciones de la vocales en el rostro, para llegar a las sÃ­labas ("consonante LPC" + "vocal LPC") y a las palabras.\r\n\r\nPara ejercicios progresivos, Listos para usar ofrece la posibilidad de trabajar solamente determinadas vocales o consonantes.\r\n\r\nPuede componer secuencias de vocales o sÃ­labas, elegir el nÃºmero de cartas (por tanto, el nÃºmero de "llaves LPC" para los ejercicios), acceder a tablas de resumen, elegir una palabra en la listaâ€¦\r\n\r\nTambiÃ©n tiene la posibilidad de elegir entre el modo silencioso o sonoro (a excepciÃ³n de los ejercicio con palabras que siempre son silenciosas).\r\n\r\nCon Listos para usar, dispone de una herramienta completa para hacer progresar rÃ¡pidamente a su hijo.\r\nInformaciones prÃ¡cticas\r\n\r\n!\r\n\r\nRequisitos previos : Para los adultos principiantes y niÃ±os sordos con sus padres\r\n\r\n5+\r\naÃ±os\r\n\r\nSegÃºn el nivel de francÃ©s del niÃ±o.\r\n\r\nPC\r\n\r\nConfiguraciÃ³n mÃ­nima : Pentium II 350 Mhz, 64Mo de RAM, Windows XP, Vista, siete y 8, tarjeta de vÃ­deo 16 Mo, tarjeta de sonido, lector DVD-ROM, pantalla 1024 x 768\r\nLa vache et le chevalier\r\nOfrece una ayuda a los niÃ±os sordos, en su conocimiento de la lengua espaÃ±ola.\r\nMeta\r\n\r\n"La Vache et le Chevalier" ofrece una ayuda a los niÃ±os sordos en su conocimiento de la lengua espaÃ±ola.\r\nÃ©cran 1 Ã©cran 2\r\n\r\n30 â‚¬\r\ninc.IVA\r\n\r\nÂ¡Compra!\r\n\r\nEl software estÃ¡ disponible unicamente en CD-ROM.\r\nDescripciÃ³n\r\n\r\nEl DVD-ROM estÃ¡ diseÃ±ado en torno a una historia estructurada en tres niveles de lenguaje, que permite a los niÃ±os progresar por etapas.\r\n\r\nRealizado con la ayuda del Ministerio de EducaciÃ³n Nacional.\r\n\r\nLa historia de â€œLa vaca y el caballeroâ€ y las partes anexas del DVD-ROM estÃ¡n subtituladas, codificadas en LPC, suscritas (Ãºnicamente para el primer nivel de lengua) e ilustradas con numerosas imÃ¡genes que ayudan a la comprensiÃ³n.\r\n\r\nEn la parte interactiva del DVD-ROM se encuentran disponibles una parte lÃ©xica y sintÃ¡ctica en referencia a la historia, asÃ­ como un tema desarrollado sobre â€œlas luces y las sombrasâ€\r\nDetalles\r\n\r\nLos â€œlibros completadosâ€ constituyen una ayuda para los profesores, proporcionando les, un buen soporte, adaptado a las necesidades de los niÃ±os con deficiencia auditiva, con el fin de favorecer el aprendizaje y dominio de la lengua espaÃ±ola. Les histÃ³rias, estÃ¡n estructuradas de manera progresiva, estÃ¡n ilustradas de una manera muy precisa e incluyen subtÃ­tulos, cÃ³digo LPC y LSF.\r\n\r\nEl DVD-ROM interactivo de â€œLa vaca y el caballeroâ€, ha sido diseÃ±ado especÃ­ficamente para los niÃ±os con sordera profunda o discapacidad auditiva, por tanto, estÃ¡n subtitulados y complementados con el cÃ³digo LPC, es un apoyo que puede ser mostrado a toda la clase.\r\n\r\nEn los â€œLibros completadosâ€ , las adaptaciones a la discapacidad de la sordera no estÃ¡n sistemÃ¡ticamente impuestas: hay diferentes opciones que permiten una flexibilidad en las modalidades ofrecidas por el docente, segÃºn el perfil del niÃ±o y segÃºn sus capacidades en la lengua espaÃ±ola y en el cÃ³digo LPC.\r\n\r\nUn complemento lÃ©xico y el desarrollo de un tema realista enriquecen la historia (aquÃ­, el tema trata sobre â€œLas luces y las sombrasâ€).\r\n\r\nFinalmente, Los â€œLibros completadosâ€ deben constituir una colecciÃ³n destinada a los alumnos con sordera profunda o discapacidad, desde infantil hasta primaria.\r\nInformaciones prÃ¡cticas\r\n\r\n!\r\n\r\nRequisitos previos : NiÃ±os pequeÃ±os con sordera profunda o discapacidad auditiva.\r\n\r\n5/10\r\naÃ±os\r\n\r\nSegÃºn el nivel de francÃ©s del niÃ±o.\r\n\r\nPC\r\n\r\nConfiguraciÃ³n mÃ­nima : Pentium II 350 Mhz, 64Mo de RAM, Windows XP, Vista, siete y 8, tarjeta de vÃ­deo 16 Mo, tarjeta de sonido, lector DVD-ROM, pantalla 1024 x 768\r\nDes logiciels adaptÃ©s pour votre enfant sourd !\r\nEl mundo sonoro de Otto\r\n\r\nEstimula el desarrollo sensorial de los niÃ±os y mejora sus facultades auditivas !saber mÃ¡s\r\n\r\nJuego de descubrimiento de los sonidos y educaciÃ³n auditiva.\r\n\r\n2+\r\naÃ±os\r\nEl mundo sonoro de Otto\r\nLa vache et le chevalier\r\n\r\nAyuda a los niÃ±os sordos en su conocimiento de la lengua francesa. Historia subtitulada y codificada en LPCsaber mÃ¡s\r\n\r\nOfrece una ayuda a los niÃ±os sordos, en su conocimiento de la lengua espaÃ±ola.\r\n\r\n5/10\r\naÃ±os\r\nLa vache et le chevalier\r\nClÃ©s en main\r\n\r\nFamiliariza eficazmente a los adultos principiantes y a los niÃ±os con el cÃ³digo LPC a travÃ©s de un juego de cartas saber mÃ¡s\r\n\r\nFamiliarizaciÃ³n con el cÃ³digo LPC\r\n\r\n5+\r\naÃ±os\r\nClÃ©s en main\r\n', 'Detail', '', 0, 0, 20);

-- --------------------------------------------------------

--
-- Structure de la table `paragraphes`
--

CREATE TABLE IF NOT EXISTS `paragraphes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) DEFAULT NULL,
  `body_fr` text,
  `body_en` text NOT NULL,
  `body_es` text NOT NULL,
  `image` text,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `productmods`
--

CREATE TABLE IF NOT EXISTS `productmods` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `weight` decimal(8,2) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modified` (`modified`),
  KEY `category_id` (`product_id`),
  KEY `brand_id` (`value`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=2 ;

--
-- Contenu de la table `productmods`
--

INSERT INTO `productmods` (`id`, `product_id`, `sku`, `name`, `value`, `price`, `weight`, `active`, `views`, `created`, `modified`) VALUES
(1, 19, 'aura_boot_8', 'Size  8 US', 'Size  8 US', '68.95', '5.00', 1, 0, '2013-10-30 00:11:30', '2013-10-30 00:11:30');

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `brand_id` int(11) unsigned DEFAULT NULL,
  `name_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_fr` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'short description',
  `about_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `about_es` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_es` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_max` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `try` tinyint(1) NOT NULL DEFAULT '0',
  `trydur` int(2) NOT NULL DEFAULT '10',
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `prompted` tinyint(1) NOT NULL DEFAULT '0',
  `cdrom` tinyint(1) NOT NULL,
  `usb` tinyint(1) NOT NULL,
  `telechargement` tinyint(1) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `mac` tinyint(1) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name_fr`),
  KEY `modified` (`modified`),
  KEY `name_slug` (`slug`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Logiciels informatique' AUTO_INCREMENT=25 ;

--
-- Contenu de la table `products`
--

INSERT INTO `products` (`id`, `category_id`, `brand_id`, `name_fr`, `name_en`, `name_es`, `slug`, `about_fr`, `about_en`, `about_es`, `description_fr`, `description_en`, `description_es`, `image`, `image_max`, `price`, `try`, `trydur`, `tags`, `views`, `active`, `created`, `modified`, `prompted`, `cdrom`, `usb`, `telechargement`, `win`, `mac`, `age`) VALUES
(18, 4, 4, 'Le monde sonore d''Otto', 'Otto''s world ', 'El mundo sonoror de Otto', 'dc-rogan-snowboard-boots-black-rasta', 'Stimule le développement sensoriel et améliore les facultés auditives !', '', '', 'Jeu de découverte sonore et d’éducation auditive.', '', '', 'img_8.png', 'img_1.2.png', '30', 0, 10, NULL, 1179, 1, '2012-12-06 00:00:00', '2016-02-10 07:34:32', 1, 0, 0, 0, 0, 0, 0),
(19, 4, 6, 'La vache et le chevalier', '', '', 'forum-aura-snowboard-boots-chocolate', 'Aide les enfants sourds dans leur maitrise de la lange Française. Histoire sous-titrée et condé en LPC. ', '', '', 'Aide à l''acquisition de la langue française. A l''attention des enseignants.', '', '', 'img_7.png', 'img_2.2.png', '302', 0, 10, NULL, 1199, 1, '2012-12-06 00:00:00', '2016-02-18 13:36:27', 1, 0, 0, 0, 0, 0, 0),
(20, 4, 4, 'Cles en main ', '', '', 'dc-lear-mittens-blue-radiance-black', '\n	\nFamiliarise efficacement les adultes débutants et les enfants au code LPC sous la forme d''un jeu de cartes. ', '', '', 'Entrainement et familiarisation au code LCP', '', '', 'img_6.png', 'img_3.2.png', '30', 0, 10, NULL, 1138, 1, '2012-12-06 00:00:00', '2016-02-10 07:33:56', 1, 0, 0, 0, 0, 0, 0),
(21, 3, NULL, 'Digivox', '', '', 'Digivox', 'L''audiométrie vocale en toute fluidité !', '', '', 'Logiciel très complet et ultra ergonomique pour la passation d''audiométries vocales avec ou sans bruit de fond, de la stéréo au 7.1 Digivox contient nativement toutes les listes validées par le Collè National d''Audioprothèse et se veut être l''outil à avoir en permanence sous la main pour la réalisation d''audiométries vocales. Petit, simple et ultra complet !\r\n', '', '', NULL, 'img-aud_1.png', '375', 1, 10, NULL, 5, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(22, 3, NULL, 'VideoShow', '', '', 'VidéoShow', 'Support logiciel à l''audiométrie pédiatrique.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_2.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(23, 3, NULL, 'La Terrasse', '', '', 'La Terrasse', 'Tests auditifs supraliminaires.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_4.png', '375', 0, 10, NULL, 1, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(24, 3, NULL, 'La souris bleue', '', '', 'La souris bleue', 'L''imagier sonore pour l''éveil auditif !', '', '', 'L''imagier sonore La Souris bleue est un logiciel d''éveil auditif et d''évaluation des capacités auditives. Il peut être utilisé comme matériel pédagogique (par les enseignants, les orthophonistes, les éducateurs spécialisés) ou comme matériel d''évaluation des capacités de reconnaissance auditive, sonore et vocale par les professionnels de l''audiophonologie.', '', '', NULL, 'img-aud_3.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `products.old`
--

CREATE TABLE IF NOT EXISTS `products.old` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `brand_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'short description',
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_max` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `try` tinyint(1) NOT NULL DEFAULT '0',
  `trydur` int(2) NOT NULL DEFAULT '10',
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `prompted` tinyint(1) NOT NULL DEFAULT '0',
  `cdrom` tinyint(1) NOT NULL,
  `usb` tinyint(1) NOT NULL,
  `telechargement` tinyint(1) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `mac` tinyint(1) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `modified` (`modified`),
  KEY `name_slug` (`slug`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Logiciels informatique' AUTO_INCREMENT=25 ;

--
-- Contenu de la table `products.old`
--

INSERT INTO `products.old` (`id`, `category_id`, `brand_id`, `name`, `slug`, `about`, `description`, `image`, `image_max`, `price`, `try`, `trydur`, `tags`, `views`, `active`, `created`, `modified`, `prompted`, `cdrom`, `usb`, `telechargement`, `win`, `mac`, `age`) VALUES
(18, 4, 4, 'Le monde sonore d''Otto', 'dc-rogan-snowboard-boots-black-rasta', '', 'this is a description', 'img_8.png', 'img_1.2.png', '30', 0, 10, NULL, 1179, 1, '2012-12-06 00:00:00', '2016-02-10 07:34:32', 1, 0, 0, 0, 0, 0, 0),
(19, 4, 6, 'La vache et le chevalier', 'forum-aura-snowboard-boots-chocolate', '', 'this is a description', 'img_7.png', 'img_2.2.png', '30', 0, 10, NULL, 1199, 0, '2012-12-06 00:00:00', '2016-02-10 07:34:14', 1, 0, 0, 0, 0, 0, 0),
(20, 4, 4, 'Cles en main ', 'dc-lear-mittens-blue-radiance-black', '', 'Entrainement et familiarisation au code LCP', 'img_6.png', 'img_3.2.png', '30', 0, 10, NULL, 1139, 1, '2012-12-06 00:00:00', '2016-02-10 07:33:56', 1, 0, 0, 0, 0, 0, 0),
(21, 3, NULL, 'Digivox', 'Digivox', 'L''audiométrie vocale en toute fluidité !', 'Logiciel très complet et ultra ergonomique pour la passation d''audiométries vocales avec ou sans bruit de fond, de la stéréo au 7.1 Digivox contient nativement toutes les listes validées par le Collè National d''Audioprothèse et se veut être l''outil à avoir en permanence sous la main pour la réalisation d''audiométries vocales. Petit, simple et ultra complet !\r\n', NULL, 'img-aud_1.png', '375', 1, 10, NULL, 24, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(22, 3, NULL, 'VideoShow', 'VidéoShow', 'Support logiciel à l''audiométrie pédiatrique.', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', NULL, 'img-aud_2.png', '375', 0, 10, NULL, 3, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(23, 3, NULL, 'La Terrasse', 'La Terrasse', 'Tests auditifs supraliminaires.', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', NULL, 'img-aud_4.png', '375', 0, 10, NULL, 1, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(24, 3, NULL, 'La souris bleue', 'La souris bleue', 'L''imagier sonore pour l''éveil auditif !', 'L''imagier sonore La Souris bleue est un logiciel d''éveil auditif et d''évaluation des capacités auditives. Il peut être utilisé comme matériel pédagogique (par les enseignants, les orthophonistes, les éducateurs spécialisés) ou comme matériel d''évaluation des capacités de reconnaissance auditive, sonore et vocale par les professionnels de l''audiophonologie.', NULL, 'img-aud_3.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=7 ;

--
-- Contenu de la table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created`, `modified`) VALUES
(1, 'clothing', '2013-11-03 15:18:13', '2013-11-03 15:18:13'),
(2, 'winter', '2013-11-03 15:18:33', '2013-11-03 15:18:33'),
(3, 'summer', '2013-11-03 15:18:36', '2013-11-03 15:18:36'),
(4, 'equipment', '2013-11-03 15:18:41', '2013-11-03 15:18:41'),
(5, 'black', '2013-11-03 15:39:35', '2013-11-03 15:39:35'),
(6, 'white', '2013-11-03 15:39:39', '2013-11-03 15:39:39');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cabinet` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profession` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` text COLLATE utf8_unicode_ci,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pays` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `civilite` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=6 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `role`, `lastname`, `type`, `username`, `cabinet`, `profession`, `email`, `password`, `rue`, `ville`, `tel`, `cp`, `pays`, `active`, `created`, `modified`, `civilite`) VALUES
(1, 'admin', 'admin', '', 'admin', '', '', '', 'b9a51ed9f3d3e5cda1db52123001a1fe77955b93', NULL, '', '', '', '', 1, '2011-09-26 00:34:07', '2016-02-22 16:13:50', ''),
(2, 'customer', 'andras', '', 'andras', '', '', '', 'd043520c9576ccc8977ba16d6343375b61f9fab3', NULL, '', '', '', '', 1, '2013-10-29 16:58:16', '2013-10-30 01:56:38', ''),
(5, 'customer', 'user', '0', 'user', '', '', 'utilisateur@yahoo.com', '04f97bf9139451a0c12a9f0d27766695e4c547c9', '', '', '', '', '', 1, '2016-02-05 12:12:17', '2016-02-18 14:43:41', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
