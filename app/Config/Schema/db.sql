-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 15 Février 2016 à 15:52
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `cakecart`
--

-- --------------------------------------------------------

--
-- Structure de la table `autres`
--

CREATE TABLE IF NOT EXISTS `autres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detail_id` int(11) NOT NULL,
  `image` varchar(22) NOT NULL,
  `prix` int(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='produits informatique tel que cd, casques,...' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=7 ;

--
-- Contenu de la table `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `active`, `created`, `modified`) VALUES
(1, 'Burton', 'burton', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(2, 'Celtek', 'celtek', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(3, 'Dakine', 'dakine', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(4, 'DC', 'dc', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(5, 'Electric', 'electric', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(6, 'Forum', 'forum', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `carts`
--

CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sessionid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` decimal(6,2) DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `weight_total` decimal(6,2) DEFAULT NULL,
  `subtotal` decimal(6,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `lft` int(10) unsigned DEFAULT NULL,
  `rght` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `lft`, `rght`, `name`, `slug`, `description`, `created`, `modified`) VALUES
(1, NULL, 1, 18, 'Main', 'main', 'main', '2016-02-05 11:47:38', '2016-02-05 11:47:38'),
(2, 1, 2, 3, 'Logiciels Famille', 'famille', 'Aidez votre enfant dÃ©ficient auditif\ndans son apprentissage de la langue', '2016-02-05 11:48:57', '2016-02-05 11:48:57'),
(3, 1, 4, 17, 'Logiciels Professionnels', 'professionnels', 'Augmentez votre efficacitÃ©,\r\ndÃ©couvrez vos nouveaux outils', '2016-02-05 11:49:46', '2016-02-05 11:49:46'),
(4, 3, 11, 12, 'ORL et Phoniatres', 'phoniatres', 'Des outils simples et ergonomiques Ã  avoir sous la main pour vos bilans audiomÃ©triques chez l''adulte et l''enfant ! ', '2016-02-09 12:52:13', '2016-02-09 12:52:13'),
(5, 3, 13, 14, 'Orthophonistes', 'orthophonistes', 'Parce que la rÃ©Ã©ducation auditive passe aussi par le jeu ! BibliothÃ¨ques sonores, jeux, apprentissages ... ', '2016-02-09 12:55:47', '2016-02-09 12:55:47'),
(6, 3, 15, 16, 'AudioprothÃ©sistes', 'audioprothesistes', 'Ayez sous la main les outils informatiques nÃ©cessaires Ã  parfaire vos bilans prÃ© et post-appareillage ! ', '2016-02-09 12:56:16', '2016-02-09 12:56:16');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `parent_id` varchar(36) NOT NULL,
  `foreign_key` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `model` varchar(40) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `title` varchar(40) NOT NULL,
  `slug` varchar(40) NOT NULL,
  `body` varchar(40) NOT NULL,
  `author_name` varchar(40) NOT NULL,
  `author_url` varchar(40) NOT NULL,
  `author_email` varchar(128) NOT NULL,
  `language` varchar(6) NOT NULL,
  `is_spam` varchar(20) NOT NULL,
  `comment_type` varchar(32) NOT NULL,
  `created` datetime(6) NOT NULL,
  `modified` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `details`
--

CREATE TABLE IF NOT EXISTS `details` (
  `category_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logiciel_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `details`
--

INSERT INTO `details` (`category_id`, `product_id`, `id`, `logiciel_id`) VALUES
(2, 18, 1, 18),
(3, 18, 2, 18),
(2, 20, 3, 20),
(2, 19, 6, 19),
(6, 21, 7, 21),
(6, 22, 8, 22),
(6, 23, 9, 23),
(6, 24, 10, 24),
(4, 21, 12, 21),
(4, 22, 13, 22);

-- --------------------------------------------------------

--
-- Structure de la table `dtmedia`
--

CREATE TABLE IF NOT EXISTS `dtmedia` (
  `product_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `logiciels`
--

CREATE TABLE IF NOT EXISTS `logiciels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `brand_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'short description',
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_max` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `try` tinyint(1) NOT NULL DEFAULT '0',
  `trydur` int(2) NOT NULL DEFAULT '10',
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `prompted` tinyint(1) NOT NULL DEFAULT '0',
  `cdrom` tinyint(1) NOT NULL,
  `usb` tinyint(1) NOT NULL,
  `telechargement` tinyint(1) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `mac` tinyint(1) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `modified` (`modified`),
  KEY `name_slug` (`slug`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Logiciels informatique' AUTO_INCREMENT=25 ;

--
-- Contenu de la table `logiciels`
--

INSERT INTO `logiciels` (`id`, `category_id`, `brand_id`, `name`, `slug`, `about`, `description`, `image`, `image_max`, `price`, `try`, `trydur`, `tags`, `views`, `active`, `created`, `modified`, `prompted`, `cdrom`, `usb`, `telechargement`, `win`, `mac`, `age`) VALUES
(18, 4, 4, 'Le monde sonore d''Otto', 'dc-rogan-snowboard-boots-black-rasta', '', 'this is a description', 'img_8.png', 'img_1.2.png', '30', 0, 10, NULL, 1179, 1, '2012-12-06 00:00:00', '2016-02-10 07:34:32', 1, 0, 0, 0, 0, 0, 0),
(19, 4, 6, 'La vache et le chevalier', 'forum-aura-snowboard-boots-chocolate', '', 'this is a description', 'img_7.png', 'img_2.2.png', '30', 0, 10, NULL, 1199, 1, '2012-12-06 00:00:00', '2016-02-10 07:34:14', 1, 0, 0, 0, 0, 0, 0),
(20, 4, 4, 'Cles en main ', 'dc-lear-mittens-blue-radiance-black', '', 'Entrainement et familiarisation au code LCP', 'img_6.png', 'img_3.2.png', '30', 0, 10, NULL, 1138, 1, '2012-12-06 00:00:00', '2016-02-10 07:33:56', 1, 0, 0, 0, 0, 0, 0),
(21, 3, NULL, 'Digivox', 'Digivox', 'L''audiométrie vocale en toute fluidité !', 'Logiciel très complet et ultra ergonomique pour la passation d''audiométries vocales avec ou sans bruit de fond, de la stéréo au 7.1 Digivox contient nativement toutes les listes validées par le Collè National d''Audioprothèse et se veut être l''outil à avoir en permanence sous la main pour la réalisation d''audiométries vocales. Petit, simple et ultra complet !\r\n', NULL, 'img-aud_1.png', '375', 1, 10, NULL, 5, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(22, 3, NULL, 'VideoShow', 'VidéoShow', 'Support logiciel à l''audiométrie pédiatrique.', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', NULL, 'img-aud_2.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(23, 3, NULL, 'La Terrasse', 'La Terrasse', 'Tests auditifs supraliminaires.', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', NULL, 'img-aud_4.png', '375', 0, 10, NULL, 1, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(24, 3, NULL, 'La souris bleue', 'La souris bleue', 'L''imagier sonore pour l''éveil auditif !', 'L''imagier sonore La Souris bleue est un logiciel d''éveil auditif et d''évaluation des capacités auditives. Il peut être utilisé comme matériel pédagogique (par les enseignants, les orthophonistes, les éducateurs spécialisés) ou comme matériel d''évaluation des capacités de reconnaissance auditive, sonore et vocale par les professionnels de l''audiophonologie.', NULL, 'img-aud_3.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `media`
--

INSERT INTO `media` (`id`, `name`) VALUES
(1, 'img1.png'),
(2, 'img2.png'),
(3, 'img3.png'),
(4, 'img4.png'),
(5, 'img5.png'),
(6, 'img6.png'),
(7, 'img7.png'),
(8, 'img8.png');

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` decimal(8,2) unsigned DEFAULT '0.00',
  `order_item_count` int(11) DEFAULT NULL,
  `subtotal` decimal(8,2) DEFAULT NULL,
  `tax` decimal(8,2) DEFAULT NULL,
  `shipping` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) unsigned DEFAULT NULL,
  `order_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorization` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `weight` decimal(8,2) unsigned DEFAULT '0.00',
  `price` decimal(8,2) unsigned DEFAULT NULL,
  `subtotal` decimal(8,2) unsigned DEFAULT NULL,
  `productmod_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `lft` int(10) NOT NULL,
  `rght` int(10) NOT NULL,
  `body_fr` text NOT NULL,
  `body_en` text NOT NULL,
  `body_es` text NOT NULL,
  `title` varchar(150) NOT NULL,
  `autor` varchar(150) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `lft`, `rght`, `body_fr`, `body_en`, `body_es`, `title`, `autor`, `active`, `product_id`) VALUES
(1, 0, 1, 2, 'Principale', 'Main', '?', 'Main', 'Admin', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `paragraphes`
--

CREATE TABLE IF NOT EXISTS `paragraphes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) DEFAULT NULL,
  `body_fr` varchar(255) DEFAULT NULL,
  `body_en` text NOT NULL,
  `body_es` text NOT NULL,
  `image` text,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `productmods`
--

CREATE TABLE IF NOT EXISTS `productmods` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `weight` decimal(8,2) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modified` (`modified`),
  KEY `category_id` (`product_id`),
  KEY `brand_id` (`value`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=2 ;

--
-- Contenu de la table `productmods`
--

INSERT INTO `productmods` (`id`, `product_id`, `sku`, `name`, `value`, `price`, `weight`, `active`, `views`, `created`, `modified`) VALUES
(1, 19, 'aura_boot_8', 'Size  8 US', 'Size  8 US', '68.95', '5.00', 1, 0, '2013-10-30 00:11:30', '2013-10-30 00:11:30');

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `brand_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'short description',
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_max` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `try` tinyint(1) NOT NULL DEFAULT '0',
  `trydur` int(2) NOT NULL DEFAULT '10',
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `prompted` tinyint(1) NOT NULL DEFAULT '0',
  `cdrom` tinyint(1) NOT NULL,
  `usb` tinyint(1) NOT NULL,
  `telechargement` tinyint(1) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `mac` tinyint(1) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `modified` (`modified`),
  KEY `name_slug` (`slug`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Logiciels informatique' AUTO_INCREMENT=27 ;

--
-- Contenu de la table `products`
--

INSERT INTO `products` (`id`, `category_id`, `brand_id`, `name`, `slug`, `about`, `description`, `image`, `image_max`, `price`, `try`, `trydur`, `tags`, `views`, `active`, `created`, `modified`, `prompted`, `cdrom`, `usb`, `telechargement`, `win`, `mac`, `age`) VALUES
(18, 4, 4, 'Le monde sonore d''Otto', 'dc-rogan-snowboard-boots-black-rasta', '', 'this is a description', 'img_8.png', 'img_1.2.png', '30', 0, 10, NULL, 1179, 1, '2012-12-06 00:00:00', '2016-02-10 07:34:32', 1, 0, 0, 0, 0, 0, 0),
(19, 4, 6, 'La vache et le chevalier', 'forum-aura-snowboard-boots-chocolate', '', 'this is a description', 'img_7.png', 'img_2.2.png', '30', 0, 10, NULL, 1199, 1, '2012-12-06 00:00:00', '2016-02-10 07:34:14', 1, 0, 0, 0, 0, 0, 0),
(20, 4, 4, 'Cles en main ', 'dc-lear-mittens-blue-radiance-black', '', 'Entrainement et familiarisation au code LCP', 'img_6.png', 'img_3.2.png', '30', 0, 10, NULL, 1138, 1, '2012-12-06 00:00:00', '2016-02-10 07:33:56', 1, 0, 0, 0, 0, 0, 0),
(21, 3, NULL, 'Digivox', 'Digivox', 'L''audiométrie vocale en toute fluidité !', 'Logiciel très complet et ultra ergonomique pour la passation d''audiométries vocales avec ou sans bruit de fond, de la stéréo au 7.1 Digivox contient nativement toutes les listes validées par le Collè National d''Audioprothèse et se veut être l''outil à avoir en permanence sous la main pour la réalisation d''audiométries vocales. Petit, simple et ultra complet !\r\n', NULL, 'img-aud_1.png', '375', 1, 10, NULL, 5, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(22, 3, NULL, 'VideoShow', 'VidéoShow', 'Support logiciel à l''audiométrie pédiatrique.', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', NULL, 'img-aud_2.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(23, 3, NULL, 'La Terrasse', 'La Terrasse', 'Tests auditifs supraliminaires.', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', NULL, 'img-aud_4.png', '375', 0, 10, NULL, 1, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(24, 3, NULL, 'La souris bleue', 'La souris bleue', 'L''imagier sonore pour l''éveil auditif !', 'L''imagier sonore La Souris bleue est un logiciel d''éveil auditif et d''évaluation des capacités auditives. Il peut être utilisé comme matériel pédagogique (par les enseignants, les orthophonistes, les éducateurs spécialisés) ou comme matériel d''évaluation des capacités de reconnaissance auditive, sonore et vocale par les professionnels de l''audiophonologie.', NULL, 'img-aud_3.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(25, 2, NULL, 'desert', NULL, '', NULL, NULL, 'Desert.jpg', '5', 0, 10, NULL, 0, NULL, '2016-02-12 08:54:37', '2016-02-12 08:54:37', 0, 0, 1, 0, 1, 0, 5),
(26, 2, NULL, 'juju', NULL, '', NULL, NULL, 'Desert.jpg', '5', 0, 10, NULL, 0, NULL, '2016-02-12 08:57:24', '2016-02-12 08:57:24', 0, 0, 1, 0, 1, 0, 5);

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=7 ;

--
-- Contenu de la table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created`, `modified`) VALUES
(1, 'clothing', '2013-11-03 15:18:13', '2013-11-03 15:18:13'),
(2, 'winter', '2013-11-03 15:18:33', '2013-11-03 15:18:33'),
(3, 'summer', '2013-11-03 15:18:36', '2013-11-03 15:18:36'),
(4, 'equipment', '2013-11-03 15:18:41', '2013-11-03 15:18:41'),
(5, 'black', '2013-11-03 15:39:35', '2013-11-03 15:39:35'),
(6, 'white', '2013-11-03 15:39:39', '2013-11-03 15:39:39');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cabinet` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profession` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` text COLLATE utf8_unicode_ci,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pays` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=6 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `role`, `lastname`, `username`, `cabinet`, `profession`, `email`, `password`, `rue`, `ville`, `tel`, `cp`, `pays`, `active`, `created`, `modified`) VALUES
(1, 'admin', 'admin', 'admin', '', '', '', 'b9a51ed9f3d3e5cda1db52123001a1fe77955b93', NULL, '', '', '', '', 1, '2011-09-26 00:34:07', '2016-02-15 14:03:44'),
(2, 'customer', 'andras', 'andras', '', '', '', 'd043520c9576ccc8977ba16d6343375b61f9fab3', NULL, '', '', '', '', 1, '2013-10-29 16:58:16', '2013-10-30 01:56:38'),
(4, 'customer', 'hasina', 'Ratinarivo', '', '', '', '992fb129fe89d9e461688a6c6e83dbd92ee0c36e', NULL, '', '', '', '', 1, '2016-02-05 11:52:37', '2016-02-05 12:09:51'),
(5, 'customer', 'user', 'user', '', '', '', '04f97bf9139451a0c12a9f0d27766695e4c547c9', NULL, '', '', '', '', 1, '2016-02-05 12:12:17', '2016-02-05 16:43:14');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
