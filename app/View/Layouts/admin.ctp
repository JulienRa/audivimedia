<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title_for_layout; ?></title>
    <?php 
        echo $this->Html->css(array(
            '/admin_menu/bootstrap/css/bootstrap.min.css',
            '/admin_menu/css/waves.min.css',
            '/admin_menu/css/style.css',
            '/admin_menu/css/menu-light.css',
            '/admin_menu/font-awesome/css/font-awesome.min.css',
            'admin.css'
            )); 
    ?>
    <?php echo $this->Html->css('acceuil_admin') ?>
        <!-- scripts -->
    <?php 
        echo $this->Html->script(array(
            'http://code.jquery.com/jquery-2.0.3.min.js',
            '//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js',
            '/admin_menu/js/metisMenu.min.js',
            '/admin_menu/js/jquery-jvectormap-1.2.2.min.js',
            '/admin_menu/js/flot/jquery.flot.js',
            '/admin_menu/js/flot/jquery.flot.tooltip.min.js',
            '/admin_menu/js/flot/jquery.flot.resize.js',
            '/admin_menu/js/flot/jquery.flot.pie.js',
            '/admin_menu/js/chartjs/Chart.min.js',
            '/admin_menu/js/pace.min.js',
            '/admin_menu/js/waves.min.js',
            '/admin_menu/js/morris_chart/raphael-2.1.0.min.js',
            '/admin_menu/js/morris_chart/morris.js',
            '/admin_menu/js/morris_chart/morris.js',
            '/admin_menu/js/jquery-jvectormap-world-mill-en.js',
            '/admin_menu/js/custom.js',
            '/admin_menu/js/chartjs/Chart.min.js'
            )) ;
    ?>
    <?php echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css') ?>
    <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js') ?>
    <style type="text/css">
        #cnt{
            margin-left: 350px;
        }
    </style>
</head>
<body>
    <section class = 'page'>
        <?php echo $this->element('admin_menu'); ?>
        <div class = '' id = 'cnt'>
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
        </div>
        <div class="">
            <div class = 'sidehomelogout'>
                <div class='sidehome'><?php echo $this->Html->link($this->Html->image('desing/home.png'),'/admin', array('escapeTitle' => false, 'title' => 'Aceuil')); ?></div>
                <div class="sidelogout"><?php echo $this->Html->link('Se deconneter', array('controller' => 'users', 'action' => 'logout', 'admin' => false)); ?></div>
            </div>
        </div>
    </section>
    
    <?php echo $this->Js->writeBuffer(); ?>
</body>
</html>

