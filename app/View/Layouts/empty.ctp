<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title_for_layout; ?></title>
	<?php echo $this->Html->css('bootstrap.min'); ?>
	<style type="text/css">
    .logo{
        width: 292px;
        height: 68.3833;
    }
    .title{
        color: #006CA3;
        font-family: Arial Helvetica sans-serif;
        text-align: center;
    }
    .ok, .inp{
        border-radius: 0px;
    }
    .login{
    	margin-top: 25%;
    }
    input{
        border-radius: 0px;
    }
    select{
        border-radius: 0px;
    }
</style>	
</head>
<body>
	<?php echo $content_for_layout; ?>
</body>
</html>
