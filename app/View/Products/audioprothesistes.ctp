<div class="body-log">
	<div class="content-log">
    	<h1 class="blue-title">
        	Logiciels pour les audioproth&eacute;sistes
        </h1>
        <p class="under-blue-title">
        	Cr&eacute;&eacute; pour les audioproth&eacute;sistes par des audioproth&eacute;sistes !
        </p>
        
<!-- PRODUCTS  -->
<?php foreach ($products as $product):?>
    <?php foreach ($product['Detail'] as $detail):?>
        <?php if($detail['category_id'] == 6): ?>    
        <div class="title-blue-back-div">
            <div class="blue-back-title">
                <?php echo $product['Product']['name']; ?>
            </div>
        </div>
        <div class="list-div-product"> 
            <?php echo $this->Html->image('/images/small/'.$product['Product']['image_max'],array('class'=>'left-img-product')); ?>
            <div class="text-right-products">
                <div class="text-right-prod-title">
                    <?php echo $this->Html->image('desing/puce_bleu.png'); ?>
                    <?php echo $product['Product']['about']; ?>
                </div>
                <div class="text-right-prod">
                    <?php echo $product['Product']['description']; ?>
                </div>
                <div class="know-more-button">
<?php echo $this->Html->link($this->Html->image('desing/puce-blue.png').' En savoir plus',array('controller' => 'products', 'action' => 'view',$product['Product']['id']),array('escapeTitle' => false, 'title' => 'plus')); ?>
                </div>
            </div>
            <div class="price-prod-right"> 
                <div class="div-for-test">
<?php if($product['Product']['try'] == 1): ?>
                    <div class="free-test">
                        <?php echo $this->Html->image('desing/fd_etoile.png'); ?>
                        <a class="btn-buy" href="#">
                        <div class="button-free-test">
                            Essayer ce logiciel
                        </div>
                        </a>
                    </div>
<?php endif; ?>
                </div>
           
                
                <div class="price-container">
                    <div class="rounded-price">
                        <div class="price-blue-content">
                            <?php echo $product['Product']['price'] ?> &euro;<br />HT
                        </div>
                    </div>
                    <br />
                    <a class="btn-buy" href="#">
                    <div class="bordered-blue-btn-buy">
                        Acheter
                    </div>
                    </a>
                </div>
                        
                    <div class="rounded-price-right">
                        <div class="price-right-content">
                            PC
                        </div>
                    </div>
            </div>
            <div style="clear:both"></div>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
<?php endforeach; ?>
    </div>
</div>

