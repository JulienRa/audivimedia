<h2>Product</h2>
<?php echo $this->Html->Image('/images/small/' . $product['Product']['image'], array('alt' => $product['Product']['name_fr'], 'class' => 'image')); ?>
<table class="table-striped ">
    <tr>
        <td>Name</td>
        <td><?php echo h($product['Product']['name_fr']); ?></td>
    </tr>
    <tr>
        <td>Description</td>
        <td><?php echo h($product['Product']['description_fr']); ?></td>
    </tr>
    <tr>
        <td>Price</td>
        <td><?php echo h($product['Product']['price']); ?></td>
    </tr>
    <tr>
        <td>Created</td>
        <td><?php echo h($product['Product']['created']); ?></td>
    </tr>
    <tr>
        <td>Modified</td>
        <td><?php echo h($product['Product']['modified']); ?></td>
    </tr>   
</table>
<?php  
    $id = $product['Product']['id'];
    $urledit = $this->Html->url(array('controller'=>'products','action'=>'edit',$id));
?>
<hr />
<h3>Actions</h3>
<a href="#" id="edit" class="btn btn-default">Editer</a>
<?php echo $this->Form->postLink('Supprimer', array('action' => 'delete', $id), array('class' => 'btn btn-danger'), __('Etes-vous sûr de vouloir supprimer # %s?', $product['Product']['id'])); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#edit').click(function(){
            $.ajax({
                 url: "<?= $urledit?>",
                 success: function(result,status){
                    $('#cnt').html(result);
                 }
            })
        });
    });
</script>