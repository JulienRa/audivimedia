

<h2>Admin Edit Product</h2>

<br />

<div class="row">
    <div class="col-sm-5">

        <?php echo $this->Form->create('Product'); ?>
        <?php echo $this->Form->input('id'); ?>
        <br />
        <?php echo $this->Form->input('category_id', array('class' => 'form-control')); ?>
        <br />
        <?php echo $this->Form->input('brand_id', array('class' => 'form-control')); ?>
        <br />
        <?php echo $this->Form->input('name', array('class' => 'form-control')); ?>
        <br />
        <?php echo $this->Form->input('slug', array('class' => 'form-control')); ?>
        <br />
        <?php echo $this->Form->input('description', array('class' => 'form-control ckeditor')); ?>
        <br />
        <?php echo $this->Form->input('image', array('class' => 'form-control')); ?>
        <br />
        <?php echo $this->Form->input('price', array('class' => 'form-control')); ?>
        <br />
        <?php echo $this->Form->input('weight', array('class' => 'form-control')); ?>
        <br />
        <?php echo $this->Form->input('active', array('type' => 'checkbox')); ?>
        <?php echo $this->Form->input('prompted', array('type' => 'checkbox')); ?>
        <br />
        <?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary')); ?>
        <?php echo $this->Form->end(); ?>

        <br />
        <br />

    </div>
</div>

<?php echo $this->Html->script('ckeditor/ckeditor', array('inline' => false)); ?>

<script type="text/javascript">

    var basePath = "<?php echo Router::url('/'); ?>";

    CKEDITOR.replace('ProductDescription', {
        filebrowserBrowseUrl : basePath + 'js/kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl : basePath + 'js/kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl : basePath + 'js/kcfinder/browse.php?type=flash',
        filebrowserUploadUrl : basePath + 'js/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl : basePath + 'js/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl : basePath + 'js/kcfinder/upload.php?type=flash'
    });

</script>