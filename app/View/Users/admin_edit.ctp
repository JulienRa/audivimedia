<h2>Modifier un utilisateur</h2>

<br />

<div >
    <div>
        <?php echo $this->Form->create('User');?>
        <?php echo $this->Form->input('email', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('type',array('type'=>'select', 'options'=>array('Monsieur','Madame','Mademoiselle'), 'label'=>'Civilité', 'class'=>'form-control')) ?>
        <fieldset >
            <legend>Profil</legend>
            <?php echo $this->Form->input('civilite',array('type'=>'select', 'options'=>array('Professionel','Particulier'), 'label'=>'Type', 'class'=>'form-control')) ?>
            <?php echo $this->Form->input('username', array('class' => 'form-control', 'label'=>'Nom: ')); ?>
            <?php echo $this->Form->input('lastname', array('class' => 'form-control', 'label'=>'Prénom: ')); ?>
        </fieldset>
        <fieldset>
            <legend>Profession</legend>
            <?php echo $this->Form->input('profession', array('class' => 'form-control')); ?>
            <?php echo $this->Form->input('cabinet', array('class' => 'form-control',)); ?>
        </fieldset>
        <fieldset>
            <legend>Coordonnées</legend>
            <?php echo $this->Form->input('rue', array('class' => 'form-control')); ?>
            <?php echo $this->Form->input('ville', array('class' => 'form-control')); ?>
            <?php echo $this->Form->input('cp', array('class' => 'form-control', 'label'=>'Code postale: ')); ?>
            <?php echo $this->Form->input('tel', array('class' => 'form-control')); ?>
            <?php echo $this->Form->input('pays', array('class' => 'form-control')); ?>
        </fieldset>
        
        <?php echo $this->Form->input('active', array('type' => 'checkbox')); ?>
        <?php echo $this->Form->input('role', array('class' => 'form-control', 'options' => array('admin' => 'admin', 'customer' => 'customer'))); ?>
        <?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary')); ?>
        
        <?php echo $this->Form->end(); ?>
    </div>
</div>