<h2>Utilisateurs</h2>
<script type="text/javascript">
    function Editer(urledit) {
        $.ajax({
            url: urledit,
            success: function(result, status){
                    $('#cnt').html(result);
                }
            });
        }
</script>
<table class="table-striped ">
    <tr>

        <th></th>
        <th></th>
        <th class="actions"></th>
    </tr>
    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo h($user['User']['username']); ?></td>
        <td>
            <?php 
            if ($user['User']['type']==0) {
                echo 'Professionel';
            }else{ 
                echo 'Particulier';
            }
            ?>
        </td>
        <td class="actions">
            <?php $urledit = $this->Html->url(array('controller'=>'users','action'=>'edit',$user['User']['id'])); ?>
            <a href="#" onclick="Editer('<?=  $urledit ?>')">Editer</a>
            <?php echo $this->Form->postLink('Supprimer', array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
            
        </td>
    </tr>
    <?php endforeach; ?>
</table>
