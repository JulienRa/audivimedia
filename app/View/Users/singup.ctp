<?php echo $this->Form->create('Singup'); ?>
<div class="container-inscrire">
   	<div class="bloc-right-inscrire">
       	 <?php echo $this->Html->image('desing/img_par.png') ?>
    </div>
   	<div class="content-inscrire">
       	<div class="titre-inscrire">
           	<?php echo __('S&rsquo;inscrire') ?>
            <div class="sep-inscrire">
            </div>
            </div>
            
            <div class="field-label">
            	<label for="username-67"><?php echo __('Email :') ?></label>
                <div class="um-clear">
                </div>
            </div>
            <div class="field-area">
              <?php echo $this->Form->input('username',array('type'=>'text','class'=>'um-form-field valid','data-validate'=>'unique_username_or_email','data-key'=>'username', 'label'=>false)) ?>
			</div>
            
            <div class="field-label2">
            	<label for="username-67"><?php echo __('Mot de passe :') ?></label>
                <div class="um-clear">
                </div>
            </div>
            <div class="field-area">
              <?php echo $this->Form->input('pswd',array('type'=>'password','id'=>'username-67','class'=>'um-form-field valid','data-validate'=>'unique_username_or_email','data-key'=>'username', 'label'=>false)) ?>
            </div>
            <div class="field-label2">
              <label for="username-67"><?php echo __('Vérification :') ?></label>
                <div class="um-clear">
                </div>
            </div>            
            <div class="field-area">
            	<?php echo $this->Form->input('pswd',array('type'=>'password','id'=>'username-67','class'=>'um-form-field valid','data-validate'=>'unique_username_or_email','data-key'=>'username', 'label'=>false)) ?>
			      </div>
            
            <div class="bouton-inscrire">
                <?php echo $this->Form->submit(__('Valider'),array('class'=>'bouton-container-inscrire')) ?>
            </div>
           <br /> <br /><br /><br />
        </div>
</div>
<?php echo $this->Form->end(); ?>