
<div class="container">
<div class="login">
<div class="row">
    <div class="row">
            <?php echo $this->Html->link($this->Html->image('desing/logo.png', array('class'=>'logo col-sm-3 col-sm-offset-4')),array('controller'=>'products', 'action'=>'index'), array('escapeTitle' => false, 'title' => 'Audivimedia')); ?>
    </div>
    <div class ='row'>
        <h4 class='col-sm-3 col-sm-offset-4 title'>ESPACE ADMINISTRATEUR</h4>
    </div>
    <div class="row">
        <div class="col-sm-3 col-sm-offset-4">
            <?php echo $this->Form->create('User', array('action' => 'login')); ?>
            <div class="form-group"><?php echo $this->Form->input('username', array('class' => 'form-control inp', 'autofocus' => 'autofocus','label'=> false,'placeholder'=>'Utilisateur')); ?></div>
            <div class="form-group"><?php echo $this->Form->input('password', array('class' => 'form-control inp', 'label' => false, 'placeholder'=>'Mot de passe')); ?></div>
            <center><?php echo $this->Form->button('Se connecter', array('class' => 'btn btn-primary ok')); ?></center>
            <?php echo $this->Form->end(); ?>
            <?php echo $this->Session->flash('flash', array('element' => 'flash_error')); ?>
        </div>
    </div>
</div>
</div>
</div>
