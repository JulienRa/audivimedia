<h2><?php echo h($log['Logiciel']['name_fr']); ?></h2>
<?php echo $this->Html->Image('/images/small/' . $log['Logiciel']['image_max'], array('alt' => $log['Logiciel']['name_fr'], 'class' => 'image')); ?>
<table class="table-striped table-condensed">
    <tr>
        <td><ul>Prix</td>
        <td><ul><?php echo h($log['Logiciel']['price']); ?></td>
    </tr>
    <tr>
        <td><ul>Active</td>
        <td><ul><?php echo $this->Html->link($this->Html->image('icon_' . $log['Logiciel']['active'] . '.png'), array('controller' => 'products', 'action' => 'switch', 'active', $log['Logiciel']['id']), array('class' => 'status', 'escape' => false)); ?></td>
    </tr>
</table>
<?php  
    $id = $log['Logiciel']['id'];
    $item = $log['Logiciel']['name_fr'];
    $urledit = $this->Html->url(array('controller'=>'logiciels','action'=>'edit',$id));
    $urlcnt = $this->Html->url(array('controller'=>'pages','action'=>'add','?'=>array('logid'=>"$id" ,'item'=> "$item")));
?>
<hr />

<script type="text/javascript">
    $(document).ready(function(){

        $('#edit').click(function(){
            $.ajax({
                 url: "<?= $urledit?>",
                 success: function(result,status){
                    $('#cnt').html(result);
                 }
            })
        });        

        $('#contents').click(function(){
            $.ajax({
                 url: "<?= $urlcnt?>",
                 success: function(result,status){
                    $('#cnt').html(result);
                 }
            })
        });

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<h2>Contenues:</h2>
<?php foreach($log['Page'] as $page): ?>
    <?php echo '<h4>'.$page['title'].'</h4>' ?>
    <?php echo "<p style='text-align: justify'>".$page['body_fr'].'</p>' ?>
<?php endforeach ?>
<hr/>
<h3>Actions</h3>
<a href="#" id="edit" class="btn btn-default">Editer</a>
<a href="#" id="contents" class="btn btn-default" data-toggle="tooltip" title='Ajouter des contenues tel que des paragraphes ou des déscriptions'>Rediger</a>
<?php echo $this->Form->postLink('Supprimer', array('action' => 'delete', $id), array('class' => 'btn btn-danger'), __('Etes-vous sûr de vouloir supprimer # %s?', $log['Logiciel']['name_fr'])); ?>