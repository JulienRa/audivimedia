

<h2>Modifier un produit</h2>

<br />

<div class="row">
    <div class="col-sm-5">

        <?php echo $this->Form->create('Logiciel'); ?>
        <?php echo $this->Form->input('id'); ?>
        <?php echo $this->Form->input('category_id', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('name', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('description', array('class' => 'form-control ckeditor')); ?>
        <?php echo $this->Form->input('image_max', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('price', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('active', array('type' => 'checkbox')); ?>
        <?php echo $this->Form->input('prompted', array('type' => 'checkbox')); ?>
        <?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary')); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<?php echo $this->Html->script('ckeditor/ckeditor', array('inline' => false)); ?>

<script type="text/javascript">

    var basePath = "<?php echo Router::url('/'); ?>";

    CKEDITOR.replace('ProductDescription', {
        filebrowserBrowseUrl : basePath + 'js/kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl : basePath + 'js/kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl : basePath + 'js/kcfinder/browse.php?type=flash',
        filebrowserUploadUrl : basePath + 'js/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl : basePath + 'js/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl : basePath + 'js/kcfinder/upload.php?type=flash'
    });

</script>
