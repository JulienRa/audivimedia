
<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<div>
	<h4><?php   echo $logiciel['Logiciel']['name_'.$lng] ?></h4>
	<?php   echo $this->Html->image('/images/small/'.$logiciel['Logiciel']['image_max'], array('alt'=>$logiciel['Logiciel']['name_'.$lng])); ?>
	<?php foreach($logiciel['Page'] as $page): ?>
		<h5><?php echo $page['title'] ?></h5>
		<p style="text-align: justify"><?php echo $page['body_'.$lng] ?></p>
	<?php endforeach; ?>
</div>
