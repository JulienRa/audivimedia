<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<div class="body-log">
	<div class="content-log">
    	<h1 class="dark-blue-title">
        	Logiciels pour les audioproth&eacute;sistes
        </h1>
        <p class="under-blue-title">
        	Cr&eacute;&eacute; pour les audioproth&eacute;sistes par des audioproth&eacute;sistes !
        </p>
        <div class="content-right-log-adaptes">
        	<p class="content-right-log-adaptes">
            	Par <b>Alain Vimet</b>, audiproth&eacute;siste membre du Coll&egrave;ge National d'Audioproth&egrave;se, &eacute;galement fondateur du centre auditif <b><i>Audivi</i></b>
            </p>
            <?php echo $this->Html->image('desing/casque.png', array('class' => 'headphone-cont-right-log-adaptes')); ?>
        </div>
        <div style="clear:both">
        </div>
        
        
        
<!-- Categorie 1 -->
<?php foreach($categories as $categorie): ?>
        <div class="title-orange-back-div">
            <div class="orange-back-title">
                <?php echo $categorie['Category']['name'] ;?>
            </div>
        </div>
        <p class="under-title-list-log">
            <?php echo $categorie['Category']['description_'.$lng]; ?>
        </p>
    <!-- Produits -->      
    <?php foreach($logiciels as $logiciel): ?>
    <?php foreach ($logiciel['Detail'] as $detail):?>
    <?php if($detail['category_id'] == 2): ?>
        <?php if($logiciel['Logiciel']['category_id'] == $categorie['Category']['id']): ?>
        <div class="div-for-prod-list-content-div">
            <div class="prod-list-content-div">
                <?php echo $this->Html->image('/images/small/'.$logiciel['Logiciel']['image_max'], array('class' => 'prod-list')); ?>
                <?php echo $this->Html->link('<p class="title-product-list-under-img">'.$logiciel['Logiciel']['name_'.$lng].'</p>',array('controller' => 'products', 'action' => 'view',$logiciel['Logiciel']['id']),array('escapeTitle' => false, 'class'=>'no-decoration')); ?>
                <p class="product-description">
                    <?php echo $logiciel['Logiciel']['description_'.$lng]; ?>
                </p>
            </div>
        </div>
        <?php endif; ?>
    <?php endif ?>       
    <?php endforeach; ?>
    <?php endforeach; ?>
    <div style="clear:both"></div>
<?php endforeach; ?>
</div>
</div>

