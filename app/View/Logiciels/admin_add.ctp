<h2>Ajouter un Logiciel</h2>
<br />

<div class="row">
    <div class="">
        <?php echo $this->Form->create('Logiciel',array('type' => 'file')); ?>
        <?php echo $this->Form->input('category_id', array('class' => 'form-control', 'label'=>'Categorie')); ?>
        <?php echo $this->Form->input('image', array('type'=>'file','onchange'=>'PreviewImage();','id'=>'uploadImage')); ?>
        <img id="uploadPreview" class="form-control" style="width: 100px; height: 100px;" />
        <fieldset>
            <legend>Nom</legend>
            <?php echo $this->Form->input('name', array('class' => 'form-control','label'=>'Français')); ?>
            <?php echo $this->Form->input('name_en', array('class' => 'form-control','label'=>'English')); ?>
            <?php echo $this->Form->input('name_es', array('class' => 'form-control','label'=>'Española')); ?>
        </fieldset>
        <?php echo $this->Form->input('age', array('class' => 'form-control')); ?>        
        <?php echo $this->Form->input('win', array('class' => 'form-control','type'=>'checkbox')); ?>
        <?php echo $this->Form->input('mac', array('class' => 'form-control','type'=>'checkbox')); ?>
        <?php echo $this->Form->input('telechargement', array('class' => 'form-control','type'=>'checkbox')); ?>
        <?php echo $this->Form->input('cdrom', array('class' => 'form-control','type'=>'checkbox')); ?>
        <?php echo $this->Form->input('usb', array('class' => 'form-control','type'=>'checkbox')); ?>
        <?php echo $this->Form->input('description', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('price', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('active', array('type' => 'checkbox')); ?>
        <?php echo $this->Form->submit('Ajouter ce Logiciel', array('class'=>'btn btn-info')); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script type="text/javascript">
    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
        oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
</script>
