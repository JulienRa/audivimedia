<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<div class="body-log">
	<div class="content-log">
    	<h1 class="blue-title-green">
        	Logiciels pour ORL et phoniatres
        </h1>
        <p class="under-blue-title">
        	Des outils simples et ergonomiques &agrave; avoir sous la main pour vos bilans audiom&eacute;triques chez l'adulte et l'enfant !
        </p>
        
<!-- PRODUCT 1 -->
<?php foreach($logiciels as $logiciel): ?>
<?php foreach ($logiciel['Detail'] as $detail):?>
    <?php if($detail['category_id'] == 4): ?>        
        <div class="title-green-back-div">
            <div class="green-back-title">
            	<?php echo $logiciel['Logiciel']['name_'.$lng]; ?>
            </div>
        </div>
        <div class="list-div-product">
            <?php echo $this->Html->image('/images/small/'.$logiciel['Logiciel']['image_max'],array('class'=>'left-img-product')); ?>
            <div class="text-right-products">
            	<div class="text-right-prod-title">
                    <?php echo $this->Html->image('desing/puce_vert.png'); ?>
                    <?php echo $logiciel['Logiciel']['about_'.$lng]; ?>
                </div>
                <div class="text-right-prod">
                	<?php echo $logiciel['Logiciel']['description_'.$lng]; ?>
                </div>
                <div class="know-more-button">
<?php echo $this->Html->image('desing/puce-blue.png').' '.$this->Js->link('En savoir plus', array('controller' => 'logiciels', 'action' => 'view',$logiciel['Logiciel']['id']),array('update'=>'#cnt','class'=>'popup-button','title'=>'popup')); ?>
                </div>
            </div>

            <div class="price-prod-right">
            	<div class="div-for-test">
<?php if($logiciel['Logiciel']['try'] == 1): ?>
                    <div class="free-test">
                        <?php echo $this->Html->image('desing/fd_etoile.png'); ?>
                        <a class="btn-buy" href="#">
                        <div class="button-free-test-green">
                            Essayer ce logiciel
                        </div>
                        </a>
                    </div>
<?php endif ?>              
                </div>
                
                <div class="price-container">
                    <div class="rounded-price-green">
                        <div class="price-blue-content">
                            <?php echo $logiciel['Logiciel']['price'];?> &euro;<br />HT
                        </div>
                    </div>
                    <br />
                    <a class="btn-buy" href="#">
                    <div class="bordered-green-btn-buy">
                        Acheter
                    </div>
                    </a>
                </div>
                        
                    <div class="rounded-price-right-green">
                        <div class="price-right-content">
                            PC
                        </div>
                    </div>
            </div>
            <div style="clear:both">
            </div>
        </div>
<?php endif ?>
<?php endforeach ?>
<?php endforeach ?>
    </div>
</div>