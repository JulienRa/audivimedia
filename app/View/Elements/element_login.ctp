<?php echo $this->Html->css('singup'); ?>
<?php echo $this->Html->css('pswdforgot'); ?>
<div class="login">
        <?php echo $this->Form->create('User', array('action' => 'login')); ?>
        <?php echo $this->Form->input('username',array('div' => 'form', 'label'=>__('Identifiant: '))); ?>
        <div style = 'clear:both'></div>
        <?php echo $this->Form->input('password',array('div' => 'form', 'label'=>__('Mot de passe: '))); ?>
        <div style = 'clear:both'></div>
        <div class="form"><a href='#'><?php echo $this->Form->button(__('Connexion')); ?></a></div>
        <div class="form-cmpt">
            <?php echo $this->Html->link(__('Créer un compte'),array('controller'=>'users','action'=>'singup')) ?>
        </div>
        <div class="form">
            <?php echo $this->Html->link(__('Mot de passe oublié'),array('controller'=>'users','action'=>'pswdforgot')) ?>
        </div>
        <?php echo $this->Form->end(); ?>
        <?php echo $this->Flash->render('flash',array('element'=>'flash_error')); ?>
        <div style="clear:both"></div>
</div>