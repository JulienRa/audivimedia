<h2>Ajouter un Contenue</h2>
<h4><?php echo $item ?></h4>

<div class="row">
    <div class="col-sm-5">
        <?php echo $this->Form->create('Page'); ?>
        <br />
        <?php echo $this->Form->input('parent_id', array('class' => 'form-control','label'=>'Categorie')); ?>
        <br />        
        <?php echo $this->Form->input('logiciel_id', array('class' => 'form-control', 'type'=>'hidden')); ?>
        <br />        
        <?php echo $this->Form->input('product_id', array('class' => 'form-control', 'type'=>'hidden')); ?>
        <br />
        <?php echo $this->Form->input('title', array('class' => 'form-control','label'=>'Titre')); ?>
        <br />
        <?php echo $this->Form->input('autor', array('class' => 'form-control','label'=>'Auteur')); ?>
        <br/>
        <?php echo $this->Form->input('body_fr', array('class' => 'form-control','label'=>'Français')); ?>
        <br/>
        <?php echo $this->Form->input('body_en', array('class' => 'form-control','label'=>'English')); ?>
        <br/>
        <?php echo $this->Form->input('body_es', array('class' => 'form-control','label'=>'Española')); ?>
        <br />        
        <?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary')); ?>
        <?php echo $this->Form->end(); ?>
        <br />
        <br />

    </div>
</div>
