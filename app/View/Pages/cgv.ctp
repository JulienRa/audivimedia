
<div class="condition-container">
    <div class="condition-content-container">
    	<div class="grand-titre">
        	Conditions g&eacute;n&eacute;rales de ventes
            <br /><img src="img/bar_hor_bleu.png" />
            <h1 style="font-size:25px; font-weight:normal; color:#4278b2">Au 08/04/2015</h1>
        </div>
        
        <div class="titre-bienvenue">
        	<img src="img/puce_bleu_fonce.png" />
            &nbsp; Bienvenue sur le site Audivimedia.fr
            </div>
            
            	<div class="text-bienvenue">
                <br />Audivimedia.fr est un site internet commercialisant en ligne des logiciels r&eacute;alis&eacute;s &agrave; destination des enfants 
                <br />malentendants et de leur entourage, ainsi que des professionnels de sant&eacute; auditive.
                
                <br /><br />Audivimedia.fr fournit les services Audivimedia.fr selon les conditions d&eacute;finies dans cette page.
                
                <br /><br />Ces conditions g&eacute;n&eacute;rales de vente r&eacute;gissent la relation contractuelle entre Audivimedia.fr et vous.
                
                
                <br /><br />Merci de lire attentivement ces conditions avant d&acute;effectuer une commande avec Audivimedia.fr.
                
                
                <br /><br />En commandant avec Audivimedia.fr, vous notifiez votre accord d&acute;&ecirc;tre soumis aux pr&eacute;sentes conditions en cochant 
                <br />l&acute;ic&ocirc;ne &laquo; J&acute;ai pris connaissance des conditions g&eacute;n&eacute;rales de vente &raquo;.
                </div>
                
          <div class="titre-mentions">
               <img src="img/puce_bleu_fonce.png" />
               &nbsp; 1. MENTIONS LEGALES
               <div class="text-mentions">
               <a href="mention-legales.php" style="font-size:18px; color:#da79c2">Voir les mentions l&eacute;gales</a>
              </div>
                    
          <div class="titre-commander" >
          	<img src="img/puce_bleu_fonce.png" />
            &nbsp;2. COMMENT COMMANDER
          </div>
          
          		<div class="text-commander">
                <br />Si vous souhaitez acheter un ou plusieurs produit(s) figurant sur le site internet, vous devez s&eacute;lectionner chaque produit 
                <br />souhait&eacute; et l&acute;ajouter &agrave; votre panier. Lorsque vous avez s&eacute;lectionn&eacute; tous les produits que vous voulez acheter, vous 
                <br />pouvez confirmer le contenu de votre panier et passer la commande. Vous devez cocher la case confirmant que vous 
                <br />avez pris connaissance des conditions g&eacute;n&eacute;rales de vente.
                
                <br /><br />A ce stade, vous serez redirig&eacute; vers une page r&eacute;capitulant les d&eacute;tails des produits que vous avez s&eacute;lectionn&eacute;s, leur prix 
                <br />et les options de livraison (incluant les frais de livraison).
                
                <br />Vous devez ensuite cliquer sur le bouton &laquo; Envoyer par e-mail &raquo; pour passer votre commande.
                <br />Vous recevrez un e-mail de demande de confirmation de votre commande. 
                <br /><br /> C&acute;est apr&egrave;s cette confirmation que le contrat est conclu.
                
                <br /><br />Apr&egrave;s avoir pass&eacute; votre commande, nous vous adressons un e-mail vous confirmant celle-ci.
                
                <br />Vous acceptez d&acute;obtenir les factures de vos achats par voie &eacute;lectronique &agrave; l&acute;adresse mail que vous aurez renseign&eacute;e 
                <br />lors de votre commande.
                </div>
                
          <div class="titre-retention">
          		<img src="img/puce_bleu_fonce.png" />
            &nbsp;3. DROIT DE RETENTION
          </div>  
          
          		<div class="text-retention">
                <br />Les produits livr&eacute;s restent la propri&eacute;t&eacute; d&acute;Audivimedia jusqu&rsquo;&agrave; leur remise au transporteur.
                </div> 
           
           <div class="titre-retractation">
                <img src="img/puce_bleu_fonce.png" />
                &nbsp;4. DROIT DE RETRACTATION
           </div>  
           
           		<div class="text-retractation">
                        <br />Pour tous les logiciels en vente via le site internet Audivimedia, vous pouvez vous r&eacute;tracter de votre commande sans donner de motif dans un d&eacute;lai de 10 jours &agrave; compter de la date de livraison des biens achet&eacute;s.
    
                        <br /><br />Pour les logiciels VideoShow et Digivox , nous avons pr&eacute;vu une p&eacute;riode d&rsquo;essai de 10 jours &agrave; l&rsquo;issue de laquelle le destinataire de la commande peut renvoyer le(s) logiciel(s) command&eacute;s. Cette possibilit&eacute; est confondue avec le d&eacute;lai de r&eacute;tractation, nonobstant le fait que le prix du logiciel ne soit pay&eacute; qu&rsquo;&agrave; l&rsquo;issue de ces 10 jours.
                        
                        <br /><br />Pour exercer votre droit de r&eacute;tractation, il vous suffit de nous retourner le(s) produit(s) concern&eacute;(s) dans le d&eacute;lai de 10 jours &agrave; compter de la livraison &agrave; l&rsquo;adresse suivante : 48, rue Montmartre, 75002 Paris en indiquant votre souhait de vous r&eacute;tracter.
                        
                        <br /><br />Si vous exercez votre droit de r&eacute;tractation dans le d&eacute;lai de 10 jours &agrave; compter de la livraison, nous vous remboursons le prix pay&eacute; lors de la commande, &agrave; l&rsquo;exclusion des frais de renvoi.
                </div> 
                
                <div class="titre-prix">
                		<img src="img/puce_bleu_fonce.png" />
            &nbsp;5. PRIX
                </div>
                
                <div class="text-prix">
                		<br />Pour chaque produit, sont indiqu&eacute;s lors de la page de commande : le prix hors taxe, le montant de la TVA et le prix toutes taxes comprises.

                        <br /><br />Sur la page de pr&eacute;sentation de chaque logiciel, le prix est indiqu&eacute; hors taxe ou toutes taxes comprises.
                        
                        <br /><br />Le prix que vous devez payer lors de la validation de la commande est le prix toutes taxes comprises.
                        
                        <br /><br />Ce prix comprend les frais de livraison indiqu&eacute;s. Les frais de livraison s&acute;&eacute;l&egrave;vent &agrave; 5&euro; pour 3 coffrets.
                </div>
                
                <div class="titre-paiement">
                	<img src="img/puce_bleu_fonce.png" />
            &nbsp;6. PAIEMENT
                </div>
                
                	<div class="texte-paiement">
                           <br /> Le fait de valider votre commande implique l&acute;obligation &agrave; votre charge de payer le prix indiqu&eacute;, sauf pour les logiciels Digivox et VideoShow. Pour ces derniers, le prix est pay&eacute; &agrave; l&acute;issue de l&acute;essai gratuit de 10 jours.
    
                    <br /><br />Les moyens de paiement accept&eacute;s par Audivimedia.fr sont les suivants :
                    
                    <br /><br />a. Ch&egrave;ques.
                    <br />b. Virements.

                    </div>
                
                 <div class="titre-livraison">
                 		<img src="img/puce_bleu_fonce.png" />
            &nbsp;7. LIVRAISON
                 </div>
                 
                 <div class="text-livraison">
                 	<br />Le d&eacute;lai de livraison est compris entre 10 et 15 jours. En tout &eacute;tat de cause, le d&eacute;lai de livraison ne saurait exc&eacute;der 30 jours.

                    <br /><br />Les produits sont livr&eacute;s &agrave; l&rsquo;adresse que vous avez indiqu&eacute;e au cours du processus de commande. En tout &eacute;tat de cause, conform&eacute;ment aux dispositions l&eacute;gales, en cas de retard de livraison, vous b&eacute;n&eacute;ficiez de la possibilit&eacute; de r&eacute;soudre le contrat dans les conditions et modalit&eacute;s d&eacute;finies &agrave; l&rsquo;Article L 138-2 du Code de la consommation reproduit ci-apr&egrave;s:
                    
                    <br /><br />"En cas de manquement du professionnel &agrave; son obligation de livraison du bien ou de fourniture du service &agrave; la date ou &agrave; l&acute;expiration du d&eacute;lai pr&eacute;vus au premier alin&eacute;a de l&rsquo;article L. 138-1 ou, &agrave; d&eacute;faut, au plus tard trente jours apr&egrave;s la conclusion du contrat, le consommateur peut r&eacute;soudre le contrat, par lettre recommand&eacute;e avec demande d&acute;avis de r&eacute;ception ou par un &eacute;crit sur un autre support durable, si, apr&egrave;s avoir enjoint, selon les m&ecirc;mes modalit&eacute;s, le professionnel d&rsquo;effectuer la livraison ou de fournir le service dans un d&eacute;lai suppl&eacute;mentaire raisonnable, ce dernier ne s&acute;est pas ex&eacute;cut&eacute; dans ce d&eacute;lai. 
                    
                    <br /><br />Le contrat est consid&eacute;r&eacute; comme r&eacute;solu &agrave; la r&eacute;ception par le professionnel de la lettre ou de l'&eacute;crit l&rsquo;informant de cette r&eacute;solution, &agrave; moins que le professionnel ne se soit ex&eacute;cut&eacute; entre-temps. 
                    
                    <br /><br />N&eacute;anmoins, le consommateur peut imm&eacute;diatement r&eacute;soudre le contrat lorsque le professionnel refuse de livrer le bien ou de fournir le service ou lorsqu&rsquo;il n&rsquo;ex&eacute;cute pas son obligation de livraison du bien ou de fourniture du service &agrave; la date ou &agrave; l&rsquo;expiration du d&eacute;lai pr&eacute;vu au premier alin&eacute;a du m&ecirc;me article L. 138-1 et que cette date ou ce d&eacute;lai constitue pour le consommateur une condition essentielle du contrat. Cette condition essentielle r&eacute;sulte des circonstances qui entourent la conclusion du contrat ou d'une demande expresse du consommateur avant la conclusion du contrat."
                    
                    <br /><br />Dans ce cas, nous proc&eacute;dons au remboursement du prix pay&eacute; au moment de la commande.
                    
                    <br /><br />En cas de livraison par un transporteur n&eacute;cessitant l&rsquo;indication d&rsquo;un jour et d&rsquo;une plage horaire de livraison, Audivimedia ne saurait &ecirc;tre tenu responsable d&rsquo;un retard ou d&eacute;faut de livraison d&ucirc; &agrave; une indisponibilit&eacute; du client au jour et &agrave; la plage horaire pr&eacute;alablement d&eacute;finis.
                    
                    <br /><br />A compter de la r&eacute;alisation de la livraison, tout risque de perte ou d&acute;endommagement des produits livr&eacute;s vous est  transf&eacute;r&eacute;.
                 </div>
                 
                 	<div class="titre-garanties">
                    	<img src="img/puce_bleu_fonce.png" />
            &nbsp;8. GARANTIES
                    </div>
                    
                    <div class="text-garanties">
                    	<br />Conform&eacute;ment aux dispositions des garanties l&eacute;gales de conformit&eacute; et des vices cach&eacute;s, nous vous remboursons ou 
                        nous vous &eacute;changeons les produits apparemment d&eacute;fectueux ou ne correspondant pas &agrave; votre commande.
                        
                       <br /><br /> Il est rappel&eacute; que dans le cadre de la garantie l&eacute;gale de conformit&eacute;, le consommateur :
                        
                        <br /><br />- b&eacute;n&eacute;ficie d&rsquo;un d&eacute;lai de deux ans &agrave; compter de la d&eacute;livrance du bien pour agir &agrave; l&rsquo;encontre de son vendeur ;
                        <br />- peut choisir entre la r&eacute;paration ou le remplacement du bien, sous r&eacute;serve des conditions de co&ucirc;t pr&eacute;vues par l&rsquo;article L 211-9 du code de la consommation ;
                        <br />- est dispens&eacute; de rapporter la preuve de l&rsquo;existence du d&eacute;faut de conformit&eacute; du bien durant les six mois suivant la 
                        d&eacute;livrance du bien. Ce d&eacute;lai est port&eacute; &agrave; 24 mois &agrave; compter du 18 mars 2016, sauf pour les biens d&rsquo;occasion.
                        
                       <br /> <br />La garantie l&eacute;gale de conformit&eacute; s&rsquo;applique ind&eacute;pendamment de la garantie commerciale pouvant &eacute;ventuellement 
                        couvrir votre bien. Il est rappel&eacute; que le consommateur peut d&eacute;cider de mettre en &oelig;uvre la garantie contre les d&eacute;fauts 
                        cach&eacute;s de la chose vendue au sens de l&rsquo;article 1644 du code civil.
                        
                        <br /><br />Les produits doivent nous &ecirc;tre retourn&eacute;s dans l&rsquo;&eacute;tat dans lequel vous les avez re&ccedil;us avec l&rsquo;ensemble des &eacute;l&eacute;ments. 
                        Nous vous remboursons alors le prix pay&eacute; lors de la commande, ainsi que les frais de renvoi sur pr&eacute;sentation des justificatifs.
                        
                        <br /><br />L&rsquo;exercice des garanties l&eacute;gales ne vous emp&ecirc;chent pas de b&eacute;n&eacute;ficier du droit de r&eacute;tractation pr&eacute;vu au paragraphe 4.

                    </div>
                    
                 <div class="titre-responsabilite">
                 	<img src="img/puce_bleu_fonce.png" />
            &nbsp;9. RESPONSABILITE
                 </div>
                 
                 <div class="text-responsabilite">
                 		<br />Les produits propos&eacute;s par Audivimedia.fr sont conformes &agrave; la l&eacute;gislation fran&ccedil;aise en vigueur. La responsabilit&eacute; 
                        d&rsquo;Audivimedia.fr ne saurait &ecirc;tre engag&eacute;e en cas de non-respect de la l&eacute;gislation du pays o&ugrave; le produit est livr&eacute;. Il vous appartient de v&eacute;rifier aupr&egrave;s des autorit&eacute;s locales les possibilit&eacute;s d&rsquo;importation ou d&rsquo;utilisation des produits ou services que vous envisagez de commander.
                        
                        <br /><br />Les photos sont communiqu&eacute;es &agrave; titre illustratif et correspondent &agrave; certaines sc&egrave;nes contenues dans les logiciels 
                        auxquels elles sont rattach&eacute;es. Nous vous invitons &agrave; vous reporter au descriptif de chaque produit pour en conna&icirc;tre les caract&eacute;ristiques pr&eacute;cises.
                        
                        <br /><br />Audivimedia.fr ne saurait &ecirc;tre responsable au titre des dommages r&eacute;sultant d&rsquo;une mauvaise utilisation de l&rsquo;appareil par le client.
                        
                        <br /><br />L&rsquo;impossibilit&eacute; totale ou partielle d&rsquo;utiliser les produits, notamment pour cause d&rsquo;incompatibilit&eacute; du mat&eacute;riel, ne peut donner lieu &agrave; aucun d&eacute;dommagement ou remboursement ou mise en cause de la responsabilit&eacute; d&rsquo;Audivimedia.fr. Il vous appartient de vous reporter au descriptif du produit afin de prendre connaissance du mat&eacute;riel requis pour son utilisation.
                        
                        <br /><br />Pour les utilisateurs professionnels, la responsabilit&eacute; d&rsquo;Audivimedia.fr ne saurait &ecirc;tre engag&eacute;e pour tout pr&eacute;judice r&eacute;sultant d&rsquo;une activit&eacute; professionnelle.
                 </div>
                 
                 	<div class="titre-litiges">
                    	<img src="img/puce_bleu_fonce.png" />
            &nbsp;10. DROIT APPLICABLE ET LITIGES
                    </div>
                    
                    <div class="text-litiges">
                   <br /> Les pr&eacute;sentes Conditions sont r&eacute;dig&eacute;es en langue fran&ccedil;aise et soumises &agrave; la loi fran&ccedil;aise.

				   <br /><br />En cas de litige, le Tribunal de commerce de Paris sera comp&eacute;tent.
                    </div>
                    
                  <div class="titre-modifications">
                  	<img src="img/puce_bleu_fonce.png" />
            &nbsp;11. MODIFICATION DES CONDITIONS GENERALES DE VENTE
                  </div>
                  
                  <div class="text-modifications">
                  	<br />Nos conditions g&eacute;n&eacute;rales de vente sont r&eacute;actualis&eacute;es r&eacute;guli&egrave;rement.

					<br /><br />La version applicable &agrave; votre achat est la derni&egrave;re version en vigueur au moment de votre commande.
                  </div>
                
                <div class="titre-donnees">
                	<img src="img/puce_bleu_fonce.png" />
            &nbsp;12. DONNEEES PERSONNELLES
                </div>
                
                <div class="text-donnees">
                	<br />Les informations nominatives personnelles vous concernant sont n&eacute;cessaires &agrave; la gestion de votre commande et &agrave; nos relations contractuelles. Elles peuvent &ecirc;tre transmises aux soci&eacute;t&eacute;s qui contribuent &agrave; ces relations telles que charg&eacute;es de l&rsquo;ex&eacute;cution des services et commandes que vous avez souhait&eacute;s pour leur gestion, ex&eacute;cution, traitement et paiement. Ces informations et donn&eacute;es sont &eacute;galement conserv&eacute;es &agrave; des fins de s&eacute;curit&eacute;, afin de respecter les obligations l&eacute;gales et r&eacute;glementaires. Nous nous engageons &agrave; ne pas les transf&eacute;rer &agrave; des fins commerciales ext&eacute;rieures aux commandes et services pass&eacute;s avec nous.

<br /><br />Conform&eacute;ment &agrave; la loi informatique et libert&eacute;s du 6 janvier 1978, vous disposez d&rsquo;un droit d&rsquo;acc&egrave;s et de rectification et d&rsquo;opposition aux donn&eacute;es personnelles vous concernant. Il vous suffit de nous &eacute;crire par mail (contact@audivimedia.fr) ou par voie postale (Audivimedia - 48 rue Montmartre - 75002 Paris).
                </div>
          </div>
    </div>
</div>