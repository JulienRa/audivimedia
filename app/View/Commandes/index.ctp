
    <div class="container-commande">
    
    	<div class="content-commande">
        
        	<div class="head-commande">
            
            	<div class="box-left">
                    Passez votre commande
                    <h1 style="font-size:24px; font-style:italic; color:#4277BD">Choisissez vos logiciels</h1>
                </div>
                
                <div class="box-center">
                    Les livraisons sont assur&eacute;es sur la France m&eacute;tropolitaine pour 5 &euro; 
                    <br />TTC pour un colis n'&eacute;xc&eacute;dant pas 300 grammes (3 coffrets).
                    <br />Voir les conditions g&eacute;n&eacute;rales de vente
                </div>
                
                <div class="box-right">
                	<?php echo $this->Html->image('desing/img_commande.png') ?>
                </div>

            </div>

<!-- VIDEO SHOW -->
    	
                <div class="content-video">
                	
                    <div class="img-video">
                    <img src="img/img2.jpg" />
                    </div>
                    
                    	<div class="video-show-content1">
                        	<div class="video-show-title">
                                <img src="img/puce_bleu_fonce.png" />
                                 &nbsp; Video Show
                            </div>
                            
                            <div class="video-text1">
                                Support logiciel &agrave; 
                                <br />l'audiom&eacute;trie 
                                <br />p&eacute;diatrique.
                            </div>
                                                        
                        </div>
                        
                        <div class="video-show-content2">
                        	<div class="video-text2">
                            	ORL et phoniatres
                                <br />Audioproth&eacute;sistes
                        	</div>
                        </div>
                        
                        <div class="qte-video">
                           	<div class="titre-qte">
                            	Quantit&eacute;<br />
                            </div>
                             <div class="select-option">
                             <select style="width:70px; height:40px; border:2px solid #4277B1; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#4277B1;">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="5">6</option>
                                <option value="5">7</option>
                                <option value="5">8</option>
                                <option value="5">9</option>
                                <option value="5">10</option>
                              </select>
                             </div>
      
                        </div>
                        
                        <div class="prix-video">
                        	<div class="titre-prix-video">
                            	Prix HT
                               <br /><h1 style="font-family:Arial, Helvetica, sans-serif; font-size:22px; color:#4277B1; font-weight:normal;">x 825 &euro;
                                <br /> = 825 &euro;</h1>
                            </div>
 
                        </div>
                        
                        <div class="prix-ttc-video">
                        	<div class="titre-prix-ttc-video">
                            	Prix TTC
                                <br /><h2 style="font-family:Arial, Helvetica, sans-serif; font-size:22px; color:#4277B1; font-weight:normal;">&nbsp;&nbsp;990 &euro;</h2>
                            </div>
                        </div>
                        
                        <div class="checkbox-video">
                           <input type="checkbox" id="test1">
                        </div>
                        <div class="text-checkbox-video">
                        	Version
                            <br />CD-Rom
                        </div>
                        <div class="liv-ht-video">
                        + 5&euro; HT + livraison
                        </div>
                </div>

   
            

  
                          

                
   <div class="text-etape">
    Passer &agrave; l&rsquo;&eacute;tape suivante
   </div>             
                  
        </div>
        
    </div>
