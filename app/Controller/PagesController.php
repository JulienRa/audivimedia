<?php /**
* 
*/
App::uses('AppController', 'Controller');
App::uses('Xml', 'Utility');
class PagesController extends AppController
{
    public $helpers = array('Js');
    public function mentions()
    {
        //mentions legales
    }
    public function cgv()
    {
        //condition générale de vente
    }
    public function contacte()
    {
        //contacte
    }
    public function commande()
    {
        //commandes
    }

    public function admin_add()
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        if ($this->request->is('post')) {
            
            $logid = $this->request->query('logid');
            $prodid = $this->request->query('prodid');
            
            if (!isset($logid)) {$logid = 0;};         
            if (!isset($prodid)) {$prodid = 0;};

            $datap = array(
            'title'=>  $this->request->data['Page']['title'],
            'autor'=>  $this->request->data['Page']['autor'],
            'body_fr'=>$this->request->data['Page']['body_fr'],
            'body_en'=>$this->request->data['Page']['body_en'],
            'body_es'=>$this->request->data['Page']['body_es'],
            'parent_id'=>  $this->request->data['Page']['parent_id'],
            'logiciel_id'=>$logid,
            'product_id'=>$prodid
            );
            $this->Page->create();
            if ($this->Page->save($datap)) {
                $this->Session->setFlash('Contenue Sauvegardé   ');
                return $this->redirect(array('controller'=>'users','action'=>'dashboard'));
            } else {
                $this->Session->setFlash('Enregistrement echoué.');
            }
        }

        $parents = $this->Page->generateTreeList(null, null, null, ' -- ');
        $d = $this->request->query['amp;item'];
        $item = $this->request->query('amp;item'); //tsy azoko le amp;
        $this->set(compact('parents','item','id'));
    }
    public function admin_index()
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $contents = $this->Page->find('all');
        $this->set(compact('contents'));
    }
    public function admin_view($id )            
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $this->layout = 'empty';
        $page = $this->Page->find('first',array(
            'conditions'=>array('Page.id'=>$id)
            ));
        $this->set(compact('page'));
    }
/////////////////////////////////////////////////////////////////////////////////////
    public function admin_acceuil()
    {
        $xml = Xml::build('conf/pages.xml',array('return' => 'domdocument'));
        if ($this->request->is('Ajax')) {
            $this->layout = 'empty';
        }
        $this->loadModel('Logiciel');
        $logiciels = $this->Logiciel->find('all', array(
            'conditions'=> array('Logiciel.prompted' => 1)
            ));
        $this->set(compact('logiciels','xml'));
    }
} ?>