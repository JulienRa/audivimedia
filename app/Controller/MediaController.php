<?php /**
* 
*/
App::uses('AppController', 'Controller');
class MediaController extends AppController
{
	
	function admin_upload()
	{
		if (($this->request->is('post') || $this->request->is('put'))) {

            $this->Img = $this->Components->load('Img');

            $newName = $this->request->data['Product']['image']['name'];

            $ext = $this->Img->ext($this->request->data['Product']['image']['name']);

            $this->Media->save(array('name'=>$newName));

            $origFile = $newName;// . '.' . $ext;
            $dst = $newName;

            $targetdir = WWW_ROOT . 'images/original';

            $upload = $this->Img->upload($this->request->data['Product']['image']['tmp_name'], $targetdir, $origFile);

            if($upload == 'Success') {
                $this->Img->resampleGD($targetdir . DS . $origFile, WWW_ROOT . 'images/large/', $dst, 800, 800, 1, 0);
                $this->Img->resampleGD($targetdir . DS . $origFile, WWW_ROOT . 'images/small/', $dst, 180, 180, 1, 0);
                $this->request->data['Product']['image'] = $dst;
            } else {
                $this->request->data['Product']['image'] = '';
            }
        }
	}
} ?>