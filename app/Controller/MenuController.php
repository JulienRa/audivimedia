<?php /**
* 
*/
class MenuController extends AppController
{
	
	function beforeFilter()
	{
		parent::beforeFilter();
		$menu = array(

        'main-menu' => array(
            array(
                'title' => 'Home',
                'url' => array('controller' => 'products', 'action' => 'index'),
                'class' => array('test', 'red')
            ), 
            array(
                'title' => 'About Us',
                'url' => '#'
            ),
        )
    );

    // For default settings name must be menu
    $this->set(compact('menu'));
	}

} ?>