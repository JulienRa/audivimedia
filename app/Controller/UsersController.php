<?php
App::uses('AppController', 'Controller');
class UsersController extends AppController {

////////////////////////////////////////////////////////////

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login');
    }
    public $helpers= array('Js');

////////////////////////////////////////////////////////////
    public function singup()
    {
        
    }
////////////////////////////////////////////////////////////
    public function pswdforgot()
    {
        # code...
    }
////////////////////////////////////////////////////////////


    public function login() {

        //echo AuthComponent::password('hasina@crt');
        $this->layout = 'empty';
        if ($this->request->is('post')) {
            if($this->Auth->login()) {

                $this->User->id = $this->Auth->user('id');
                $this->User->saveField('logins', $this->Auth->user('logins') + 1);
                $this->User->saveField('last_login', date('Y-m-d H:i:s'));

                if ($this->Auth->user('role') == 'customer') {
                    return $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'dashboard',
                        'customer' => true,
                        'admin' => false
                    ));
                } elseif ($this->Auth->user('role') == 'admin') {
                    $uploadURL = Router::url('/') . 'app/webroot/upload';
                    $_SESSION['KCFINDER'] = array(
                        'disabled' => false,
                        'uploadURL' => $uploadURL,
                        'uploadDir' => ''
                    );
                    return $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'dashboard',
                        'manager' => false,
                        'admin' => true
                    ));
                } else {

                    $this->Session->setFlash(Configure::read('Messages.LOG_ERROR'));
                }
            } else {
                $this->redirect('/');
                $this->Session->setFlash(Configure::read('Messages.LOG_ERROR'));
            }
        }
        $this->set('title_for_layout', 'Administrateur | '.Configure::read('Settings.SHOP_TITLE'));
    }

////////////////////////////////////////////////////////////

    public function logout() {
        $this->Session->setFlash(Configure::read('Messages.LOGOUT'));
        $_SESSION['KCEDITOR']['disabled'] = true;
        unset($_SESSION['KCEDITOR']);
        $this->Auth->logout();
        $this->redirect(array('controller'=>'logiciels','action'=>'index')); //don't use Auth redirect
    }

////////////////////////////////////////////////////////////

    public function customer_dashboard() {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
    }

////////////////////////////////////////////////////////////

    public function admin_dashboard() {
        $this->loadModel('Logiciel');
        $this->loadModel('Product');
        $logiciels = $this->Logiciel->find('all');
        $products = $this->Product->find('all');
        $this->set(compact('logiciels','products'));
        $this->set('title_for_layout', 'Administrateur | '.Configure::read('Settings.SHOP_TITLE'));
    }

////////////////////////////////////////////////////////////

    public function admin_index() {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $this->Paginator = $this->Components->load('Paginator');

        $this->Paginator->settings = array(
            'User' => array(
                'recursive' => -1,
                'contain' => array(
                ),
                'conditions' => array(
                ),
                'order' => array(
                    'Users.name' => 'ASC'
                ),
                'limit' => 20,
                'paramType' => 'querystring',
            )
        );
        $users = $this->Paginator->paginate();
        $this->set(compact('users'));
        $this->set('title_for_layout', "Utilisateur | ".Configure::read('Settings.SHOP_TITLE'));
    }

////////////////////////////////////////////////////////////

    public function admin_view($id = null) {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException('Invalid user');
        }
        $this->set('user', $this->User->read(null, $id));
    }

////////////////////////////////////////////////////////////

    public function admin_add() {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Sauvegarde éffectué');
                return $this->redirect(array('action'=>'dashboard'));
            } else {
                $this->Session->setFlash('Impossible de sauvegarder l\'enregisrement');
            }
        }
        $this->set('title_for_layout', "Utilisateur | ".Configure::read('Settings.SHOP_TITLE'));
    }

////////////////////////////////////////////////////////////

    public function admin_edit($id = null) {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException('Invalid user');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Modification éffectué');
                return $this->redirect(array('action'=>'dashboard'));
            } else {
                $this->Session->setFlash('Impossible d\'éffectué la Modification');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
        }
    }

////////////////////////////////////////////////////////////

    public function admin_password($id = null) {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException('Invalid user');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Modification éffectué');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Impossible d\'éffectué la Modification');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
        }
    }

////////////////////////////////////////////////////////////

    public function admin_delete($id = null) {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException('Invalid user');
        }
        if ($this->User->delete()) {
            $this->Session->setFlash('Utilisateur supprimé');
            return $this->redirect(array('action'=>'dashboard'));
        }
        $this->Session->setFlash('Sppression impossible');
        return $this->redirect(array('action' => 'index'));
    }

////////////////////////////////////////////////////////////

}
